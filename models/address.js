/**
 * Created by idanhahn on 10/16/2016.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    block_type: {type: String, require: true},
    street: {type: String,require: true},
    house: {type: String,require: true},
    entry: {type: String,require: true},
    apartment: {type: String,require: true},
    city: {type: String,require: true}
});

module.exports = mongoose.model('Address', schema);
