// assets/app/why/why.component.ts
import { Component } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: 'my-why',
    templateUrl: 'why.component.html',
    styleUrls:['why.component.css']
})
export class WhyComponent {
}