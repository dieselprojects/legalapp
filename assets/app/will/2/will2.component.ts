/**
 * Created by idanhahn on 10/4/2016.
 */
import { Component } from "@angular/core";
import {FormGroup, FormControl} from "@angular/forms";

@Component({
    moduleId: module.id,
    selector: 'my-will2',
    templateUrl: 'will2.component.html',
    styleUrls:['will2.component.css']
})
export class Will2Component {
    will2Form: FormGroup;
    
    constructor(){
        this.will2Form = new FormGroup({
            'user-firstName': new FormControl(''),
            'user-lastName': new FormControl('')
        })
    }
    
    onSubmit(){
        console.log(this.will2Form);
    }
    
    
    
}
