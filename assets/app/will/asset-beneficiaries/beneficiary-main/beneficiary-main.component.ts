/**
 * Created by idanhahn on 10/21/2016.
 */

import {Component, OnInit, Input} from "@angular/core";
import {BeneficiaryService} from "../beneficiary.service";
import {FormGroup, FormControl} from "@angular/forms";
import {Beneficiary} from "../../models/beneficiary";

@Component({
    moduleId: module.id,
    selector: 'app-beneficiary-main',
    templateUrl: 'beneficiary-main.component.html',
    styleUrls: ['beneficiary-main.component.css']
})

export class BeneficiaryMainComponent implements OnInit{
  
    // holds all beneficiaries:
    beneficiariesNames: Array<string>;
    
    // Add Beneficiary:
    // ----------------
    addBeneficiary: boolean = false;
    
    // New Beneficiary:
    // ----------------
    newBeneficiary: boolean = false;
    form: FormGroup;
    
    // Set Beneficiary
    setBeneficiary: boolean = false;
    displayedBeneficiary: Beneficiary;
    
    
    // private elements for forms:
    genders =[
        'זכר',
        'נקבה'
    ];
    
    
    constructor(private beneficiaryService: BeneficiaryService){

        this.form = new FormGroup({
            // Basic details:
            'first_name': new FormControl(''),
            'last_name': new FormControl(''),
            'id': new FormControl(''),
            'gender': new FormControl(''),
            'birth_date': new FormControl(''),
            // address:
            'street': new FormControl(''),
            'house': new FormControl(''),
            'entry': new FormControl(''),
            'apartment': new FormControl(''),
            'city': new FormControl('')
            
        })
    }
    
    ngOnInit():void {
        
        this.beneficiariesNames = this.beneficiaryService.getBeneficiariesNames();
        
    }
    
    // -------------------------- //
    // -- ADD MAIN BENEFICIARY -- //
    // -------------------------- //
    
    addBeneficiaryFunc(){
        this.addBeneficiary = true;
    }
    
    // select main beneficiary or -1 for new beneficiary    
    onSelect(index: string){
       
        if (index == "-1"){
            this.newBeneficiaryFunc();
        } else {
            this.setBeneficiaryFunc(this.beneficiaryService.getBeneficiary(Number(index)))
        }
        
    }
    
    // --------------------- //
    // -- New Beneficiary -- //
    // --------------------- //
    
    newBeneficiaryFunc(){
        this.setBeneficiary = false;
        this.newBeneficiary = true;
    }

    onSubmit(){
        console.log('In OnSubmit');
        console.log(this.form);

        

        
        this.setBeneficiaryFunc(this.createBeneficiary(this.form));
        
    }
    
    
    createBeneficiary(form: FormGroup): Beneficiary{
        let beneficiary: Beneficiary = new Beneficiary();
       
        //store to DB
        
        return beneficiary; 
    }
    
    
    
    // --------------------- //
    // -- Set beneficiary -- //
    // --------------------- //
    
    setBeneficiaryFunc(beneficiary: Beneficiary){
       
        this.newBeneficiary = false;
        this.displayedBeneficiary = beneficiary;
        this.setBeneficiary = true;
        
    }
    
    
}
