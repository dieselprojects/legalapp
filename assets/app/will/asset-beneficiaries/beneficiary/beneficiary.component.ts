import {Component, OnInit, Input} from "@angular/core";
import {Beneficiary} from "../../models/beneficiary";
/**
 * Created by idanhahn on 10/21/2016.
 */

@Component({
    moduleId: module.id,
    selector: 'app-beneficiary',
    templateUrl: 'beneficiary.component.html',
    styleUrls: ['beneficiary.component.css']
})
export class BeneficiaryComponent implements OnInit {
    @Input() beneficiary: Beneficiary;
    
    ngOnInit():void {
    }
    
}
