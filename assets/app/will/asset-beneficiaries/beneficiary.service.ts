import {Injectable} from "@angular/core";
import {Beneficiary} from "../models/beneficiary";
import {Address} from "../models/address";
/**
 * Created by idanhahn on 10/21/2016.
 */

@Injectable()
export class BeneficiaryService {

    private beneficiaries:{ [key:string]:Beneficiary} = {};
    
    // (create new for debug only)
    createNew(){
        
        let address = new Address();
        address.block_type = "address_0";
        address.street = "דרך השלום";
        address.house = "81";
        address.entry = "1";
        address.apartment = "ב";
        address.city = "תל אביב, יפו";
        
        let beneficiary = new Beneficiary();
        beneficiary.block_type = "beneficiary_main_female_0";
        beneficiary.first_name = "מורן";
        beneficiary.last_name = "פוסק";
        beneficiary.id = "66554654654";
        beneficiary.birth_date = new Date();
        beneficiary.relation = "אישה";
        beneficiary.address = address;


        let address2 = new Address();
        address.block_type = "address_0";
        address.street = "דרך השלום";
        address.house = "81";
        address.entry = "1";
        address.apartment = "ב";
        address.city = "תל אביב, יפו";

        let beneficiary2 = new Beneficiary();
        beneficiary2.block_type = "beneficiary_main_male_0";
        beneficiary2.first_name = "עידן";
        beneficiary2.last_name = "האן";
        beneficiary2.id = "037493509";
        beneficiary2.birth_date = new Date();
        beneficiary2.relation = "אח";
        beneficiary2.address = address2;
        
        this.beneficiaries[beneficiary.getFullName()] = beneficiary;
        this.beneficiaries[beneficiary2.getFullName()] = beneficiary2;
    }
    
    constructor(){
        this.createNew();
    }
    
    
    getBeneficiaries():any {
        return this.beneficiaries;
    }

    addBeneficiary(key: string, beneficiary:Beneficiary) {
        
        this.beneficiaries[key] = beneficiary;
        
    }
    
    getBeneficiariesNames(): Array<string>{
      
        let names: Array<string> = [];

        for (let beneficiary in this.beneficiaries){
            names.push(beneficiary);
        }
        
        return names;
        
    }
   
    getBeneficiary(index: number){

        let names: Array<string> = [];

        for (let beneficiary in this.beneficiaries){
            names.push(beneficiary);
        }
        
        return this.beneficiaries[names[index]];
        
    }
    
}