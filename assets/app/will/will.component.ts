/**
 * Created by idanhahn on 10/4/2016.
 */
import { Component } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: 'my-will',
    templateUrl: 'will.component.html',
    styleUrls:['will.component.css']
    
})
export class WillComponent {
}
