import {Routes} from "@angular/router";
import {Will0Component} from "./0/will0.component";
import {Will1Component} from "./1/will1.component";
/**
 * Created by idanhahn on 10/4/2016.
 */

export const WILL_ROUTES : Routes = [
    { path: '', component: Will0Component },
    { path: '0', component: Will0Component },
    { path: '1', component: Will1Component }
];