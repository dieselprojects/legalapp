/**
 * Created by idanhahn on 10/4/2016.
 */
import { Component } from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {WillService} from "../will.service";
//import {Will0FormObject} from "../models/formObj/will-0-form";

@Component({
    moduleId: module.id,
    selector: 'my-will0',
    templateUrl: 'will0.component.html',
    styleUrls:['will0.component.css']
})
export class Will0Component {
    will0Form: FormGroup;
    genders =[
        'זכר',
        'נקבה'
    ];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private willService: WillService
    ){
        this.will0Form = new FormGroup({
            'user-firstName' : new FormControl(''),
            'user-lastName' : new FormControl(''),
            'user-gender' : new FormControl('זכר'),
            'user-idNumber' : new FormControl(''),
            'user-email' : new FormControl('', [
                Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
            ])
        });
    }
    
    
    onSubmit(){
        console.log(this.will0Form);
        console.log(this.will0Form.get('user-firstName').value);
        /*
        let will0FormObj: Will0FormObject = new Will0FormObject(
            this.will0Form.get('user-firstName').value,
            this.will0Form.get('user-lastName').value,
            this.will0Form.get('user-idNumber').value,
            this.will0Form.get('user-gender').value,
            this.will0Form.get('user-email').value
        );
        
        this.willService.addWill0Form(will0FormObj);
        */
        this.router.navigate(['will','1']);
        
    }
}
