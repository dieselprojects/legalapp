import {Component, Input, OnInit} from "@angular/core";
import {AssetFormDescriptor} from "../../asset/asset-form-descriptor";
import {FormGroup, FormControl} from "@angular/forms";
import {Asset} from "../../models/asset";
import {AssetDescription} from "../../models/asset-description";
import {Will1Service} from "../will1.service";
import {Address} from "../../models/address";
import {LegalAddress} from "../../models/legal-address";
import {Beneficiary} from "../../models/beneficiary";
import {BeneficiaryService} from "../../asset-beneficiaries/beneficiary.service";
/**
 * Created by idanhahn on 10/20/2016.
 */


@Component({
    moduleId: module.id,
    selector: 'will1-asset-form',
    templateUrl: 'will1-asset-form.component.html',
    styleUrls: ['will1-asset-form.component.css']
})
export class Will1AssetFormComponent implements OnInit {

    @Input() formDescriptor:AssetFormDescriptor;

    assetForm:FormGroup;

    // value for asset selection dropdown button
    selectedAssetHeb:string = "בחר סוג נכס";

    // asset selection drop down values
    mainTypeOptionsHeb:Array<string>;
    
    // value for ngSwitch statement
    switch_value: string;

    
    // starts main beneficiary flow
    clickedMainBeneficiary: boolean = false;
    
    beneficiaries: Beneficiary[];
    
    constructor(private benficiaryService: BeneficiaryService , 
                private will1Service:Will1Service) {
        console.log("In will1 asset form C");
        this.assetForm = new FormGroup({
            'street': new FormControl(''),
            'house': new FormControl(''),
            'entry': new FormControl(''),
            'apartment': new FormControl(''),
            'city': new FormControl(''),
            'group': new FormControl(''),
            'section': new FormControl(''),
            'sub_section': new FormControl(''),
            'other': new FormControl('')
        })
    }

    ngOnInit():void {

        this.mainTypeOptionsHeb = this.formDescriptor.getAssetTypesHeb()

        // get all possible beneficiaries
        this.beneficiaries = this.benficiaryService.getBeneficiaries();
        
        
    }

    onSelect(selectedIndex:String) {
        
        console.log("in on select");
        console.log(selectedIndex);
        this.switch_value = this.switch(selectedIndex);
    }


    switch(selectedIndex:String):string {
        
        switch (selectedIndex) {
            case "0": // apartment
                return "option1";
                break;
            case "1": // general house
                return "option2";
                break;
            case "2": // 
                return "option1";
                break;
            case "3":
                return "option3";
                break;
            case "4":
                return "option3";
                break;
            case "5":
                return "option4";
                break;
            case "6":
                return "option4";
                break;
            case "7":
                return "option4";
                break;
            case "8":
                return "option5";
                break;
            default:

        }

    }

    onSubmit() {

        console.log("Submit Asset:")
        let asset:Asset = this.createAsset("מקרקעין", this.selectedAssetHeb, this.assetForm);

        // check asset type
        // check if main beneficiary exists and if other beneficiaries exists
        this.will1Service.addAsset(asset);
        
        // reset form
        this.resetForm();

        console.log(asset);

    }

    resetForm(){
        this.assetForm.reset();
        this.selectedAssetHeb = "בחר סוג נכס";
        this.switch_value = null;
    }

    
    // ------------------- //
    // -- Asset Handler -- //
    // ------------------- //

    createAsset(asset_type:string, asset_sub_type:string, form:FormGroup):Asset {

        let asset:Asset = new Asset();
        asset.block_type = this.getAssetBlockType(form);
        asset.asset_type = asset_type;
        asset.asset_sub_type = asset_sub_type;
        asset.asset_description = this.createAssetDescription(form);
        // TODO: implement both
        // asset.main_beneficiary = this.createMainBeneficiary(form);
        //asset.beneficiaries = this.createBeneficiaries(form);
        return asset;

    }

    // get asset block type
    getAssetBlockType(form:FormGroup):string {

        // TODO: add logic here
        // currently returning asset_0 (main beneficiary with additional beneficiaries)
        return "asset_0";

    }

    createAssetDescription(form:FormGroup):AssetDescription {

        let assetDescription:AssetDescription = new AssetDescription();
        assetDescription.block_type = this.getAssetDescriptionBlockType(this.assetForm);
        assetDescription.address = this.createAddress(form);
        assetDescription.legal_address = this.createLegalAddress(form);
        return assetDescription;

    }

    getAssetDescriptionBlockType(form:FormGroup):string {

        // TODO: add logic here

        // currentlly returing asset_description_realestate_apartment
        return "asset_description_realestate_apartment";

    }

    createAddress(form:FormGroup):Address {
        let address:Address = new Address();
        address.block_type = this.getAddressBlockType(form);
        address.street = form.value.street;
        address.apartment = form.value.apartment;
        address.entry = form.value.entry;
        address.house = form.value.house;
        address.city = form.value.city;
        return address;
    }

    getAddressBlockType(form:FormGroup):string {

        // TODO: add logic here

        // currentlly returing address_0
        return "address_0";

    }

    createLegalAddress(form:FormGroup):LegalAddress {
        let legalAddress:LegalAddress = new LegalAddress();
        legalAddress.block_type = this.getLegalAddressBlockType(form);
        legalAddress.group = form.value.group;
        legalAddress.section = form.value.section;
        legalAddress.sub_section = form.value.sub_section;
        return legalAddress;
    }

    getLegalAddressBlockType(form:FormGroup):string {

        // TODO: add logic here

        // currentlly returing address_0
        return "legal_address";

    }

}
