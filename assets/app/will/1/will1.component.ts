/**
 * Created by idanhahn on 10/4/2016.
 */
import {Component, OnInit} from "@angular/core";
import {FormGroup, FormControl} from "@angular/forms";
import {WillService} from "../will.service";
import {Asset} from "../models/asset";
import {AssetType} from "../asset/asset-type";
import {AssetFormDescriptor} from "../asset/asset-form-descriptor";
import {Will1Service} from "./will1.service";

@Component({
    moduleId: module.id,
    selector: 'my-will1',
    templateUrl: 'will1.component.html',
    styleUrls: ['will1.component.css']
})
export class Will1Component implements OnInit{


    sectionForm:FormGroup;
    
    assetFormDescriptor: AssetFormDescriptor = new AssetFormDescriptor();

    assets: Asset[];    

    constructor(private willService:WillService, private will1Service: Will1Service) {
        this.sectionForm = new FormGroup({
            'street': new FormControl(''),
            '': new FormControl('')
        })
        this.assetFormDescriptor.title = "נכס מקרקעין"

        this.assetFormDescriptor.mainTypes = [
            new AssetType("house0", "דירה"),
            new AssetType("house1", "בית פרטי"),
            new AssetType("house2", "בית"),
            new AssetType("store0", "חנות"),
            new AssetType("office0", "משרד"),
            new AssetType("field0", "שטח"),
            new AssetType("field1", "שטח חקלאי"),
            new AssetType("field2", "חניה"),
            new AssetType("other", "אחר")
        ];
        console.log("in C will1Component")
        console.log(this.assetFormDescriptor);
    }

    ngOnInit():void {
        
       this.assets = this.will1Service.getAssets(); 
        
    }

    
    // new implementation

    submitSection(){
        
    }


    onSubmit() {

        // create new Asset
        // if user logged in save asset to DB


        console.log(this.sectionForm);


    }

    
    
    
    
    
    
}
