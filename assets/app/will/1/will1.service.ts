import {Injectable} from "@angular/core";
import {Asset} from "../models/asset";
/**
 * Created by idanhahn on 10/20/2016.
 */


@Injectable()
export class Will1Service {
    // TODO: consider changing to SectionService
    assets:Asset[] = [];
    
    
    getAssets(): Asset[]{
        return this.assets;
    }
    
    
    addAsset(asset: Asset){
        console.log("Will1Service: addAsset");
        this.assets.push(asset);
        console.log(this.assets);
    }
    
}
