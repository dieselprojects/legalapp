/**
 * Created by idanhahn on 10/4/2016.
 */
import { Component } from "@angular/core";
import {FormGroup, FormControl} from "@angular/forms";

@Component({
    moduleId: module.id,
    selector: 'my-will4',
    templateUrl: 'will4.component.html',
    styleUrls:['will4.component.css']
})
export class Will4Component {
    will4Form: FormGroup;
    
    constructor(){
        this.will4Form = new FormGroup({
            'user-firstName': new FormControl(''),
            'user-lastName': new FormControl('')
        })
    }
    
    onSubmit(){
        console.log(this.will4Form);
    }
    
    
    
}
