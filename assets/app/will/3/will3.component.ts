/**
 * Created by idanhahn on 10/4/2016.
 */
import { Component } from "@angular/core";
import {FormGroup, FormControl} from "@angular/forms";

@Component({
    moduleId: module.id,
    selector: 'my-will3',
    templateUrl: 'will3.component.html',
    styleUrls:['will3.component.css']
})
export class Will3Component {
    will3Form: FormGroup;
    
    constructor(){
        this.will3Form = new FormGroup({
            'user-firstName': new FormControl(''),
            'user-lastName': new FormControl('')
        })
    }
    
    onSubmit(){
        console.log(this.will3Form);
    }
    
    
    
}
