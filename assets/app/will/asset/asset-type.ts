/**
 * Created by idanhahn on 10/20/2016.
 */

export class AssetType{
    type_en: string;
    type_heb: string;
    
    constructor(public type_en: string, public type_heb: string){};
}