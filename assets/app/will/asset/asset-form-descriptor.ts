import {AssetType} from "./asset-type";
/**
 * Created by idanhahn on 10/20/2016.
 */

export class AssetFormDescriptor{
    
    title: string;
    mainTypes: AssetType[];

    constructor(){}
    
    getAssetTypesHeb(){
        let mainAssetsHeb: Array<string> = [];
        for(let assetType of this.mainTypes){
            mainAssetsHeb.push(assetType.type_heb);
        }
        return mainAssetsHeb;
    }
    
    getAssetTypeEn(assetHeb: string){
        for(let assetType of this.mainTypes){
            if (assetType.type_heb == assetHeb){
                return assetType.type_en;
            }
        }
        
    }
    
}
