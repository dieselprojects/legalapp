import {AssetDescription} from "./asset-description";
/**
 * Created by idanhahn on 10/20/2016.
 */


export class Asset{
   
    block_type: string;
    asset_type: string;
    asset_sub_type: string;
    asset_description: AssetDescription;
    //main_beneficiary: BeneficiaryMain;
    //beneficiaries: [Beneficiary];
}