import {Address} from "./address";
import {LegalAddress} from "./legal-address";
/**
 * Created by idanhahn on 10/20/2016.
 */

export class AssetDescription{
    
    block_type: string;
    address: Address;
    legal_address: LegalAddress;
    
}
