import {Asset} from "./asset";
/**
 * Created by idanhahn on 10/20/2016.
 */

export class Section{
    
    block_type: string;
    section_type: string;
    section_heading: string;
    assets: Asset[];
    
}
