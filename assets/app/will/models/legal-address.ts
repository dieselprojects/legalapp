/**
 * Created by idanhahn on 10/20/2016.
 */

export class LegalAddress{
    
    block_type: string;
    group: string;
    section: string;
    sub_section: string;
    
}
