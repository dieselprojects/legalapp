/**
 * Created by idanhahn on 10/4/2016.
 */
export class Address {

    block_type: string;
    street: string;
    house: string;
    entry: string;
    apartment: string;
    city: string;

}
