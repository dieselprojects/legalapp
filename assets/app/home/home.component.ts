// assets/app/home/home.component.ts
import {Component, Output} from "@angular/core";
import {HttpService} from "./http.service";

@Component({
	moduleId: module.id,
	selector: 'my-home',
	templateUrl: 'home.component.html',
	styleUrls:['home.component.css']
})
export class HomeComponent {

    willExists: boolean; 
    willObj;
    willPdfStr: string;
    
	constructor(private httpService: HttpService){
        this.willExists = false;
        this.willObj = null;
    }
	
	onSubmit(){
        this.httpService.generateWill().subscribe(
            (data) => {
                this.willObj = data.json().obj;
                console.log(this.willObj);
                console.log(this.willObj.willPdf);
                this.willPdfStr = this.willObj.willPdf;
                this.willExists = true;
                console.log(this.willExists);
            }
        );
	}
    
    update(){
        this.willExists = true;
    }
    
    pdf(){
        return this.willPdfStr;
    }
	
}