import {NgModule, provide} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {LocationStrategy, HashLocationStrategy} from '@angular/common';

import {AppComponent} from "./app.component";

import {HeaderComponent} from "./header/header.component";

import {routing} from "./app.routing";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {WillComponent} from "./will/will.component";
import {Will1Component} from "./will/1/will1.component";
import {Will0Component} from "./will/0/will0.component";
import {WillService} from "./will/will.service";
import {HttpService} from "./home/http.service";
import {WillAssetComponent} from "./will/will-asset.component";
import {Will1AssetFormComponent} from "./will/1/asset-form/will1-asset-form.component";
import {Will1Service} from "./will/1/will1.service";
import {BeneficiaryMainComponent} from "./will/asset-beneficiaries/beneficiary-main/beneficiary-main.component";
import {BeneficiaryComponent} from "./will/asset-beneficiaries/beneficiary/beneficiary.component";
import {BeneficiaryService} from "./will/asset-beneficiaries/beneficiary.service";

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        WillComponent,
        Will0Component,
        Will1Component,
        WillAssetComponent,
        Will1AssetFormComponent,
        BeneficiaryMainComponent,
        BeneficiaryComponent
    ],
    imports: [BrowserModule, routing, FormsModule, ReactiveFormsModule, HttpModule],
    bootstrap: [AppComponent],
    providers: [provide(LocationStrategy, {useClass: HashLocationStrategy}), WillService, HttpService, Will1Service, BeneficiaryService]
})
export class AppModule {

}