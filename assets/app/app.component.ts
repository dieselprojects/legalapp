import { Component } from '@angular/core';
import {WillService} from "./will/will.service";

@Component({
    selector: 'my-app',
    template: ` 
        <my-header></my-header>
        <router-outlet ></router-outlet>
    `,
    styles:[`
        @font-face{
	font-family:'almoni-dl';
	font-weight:700; /*(bold)*/
	font-style: normal;
	src: url('./res/fonts/almoni-dl-aaa-700.eot');
	src: url('./res/fonts/almoni-dl-aaa-700.eot#iefix') format('embedded-opentype'),
		url('./res/fonts/almoni-dl-aaa-700.woff') format('woff'),
		url('./res/fonts/almoni-dl-aaa-700.ttf') format('truetype');
}
@font-face{
	font-family:'almoni-dl';
	font-weight:900; /*(black)*/
	font-style: normal;
	src: url('./res/fonts/almoni-dl-aaa-900.eot');
	src: url('./res/fonts/almoni-dl-aaa-900.eot#iefix') format('embedded-opentype'),
		url('./res/fonts/almoni-dl-aaa-900.woff') format('woff'),
		url('./res/fonts/almoni-dl-aaa-900.ttf') format('truetype');
}
@font-face{
	font-family:'almoni-dl';
	font-weight:300; /*(light)*/
	font-style: normal;
	src: url('./res/fonts/almoni-dl-aaa-300.eot');
	src: url('./res/fonts/almoni-dl-aaa-300.eot#iefix') format('embedded-opentype'),
		url('./res/fonts/almoni-dl-aaa-300.woff') format('woff'),
		url('./res/fonts/almoni-dl-aaa-300.ttf') format('truetype');
}
@font-face{
	font-family:'almoni-dl';
	font-weight:400; /*(regular)*/
	font-style: normal;
	src: url('./res/fonts/almoni-dl-aaa-400.eot');
	src: url('./res/fonts/almoni-dl-aaa-400.eot#iefix') format('embedded-opentype'),
		url('./res/fonts/almoni-dl-aaa-400.woff') format('woff'),
		url('./res/fonts/almoni-dl-aaa-400.ttf') format('truetype');
}
        
        
        body{
            margin: 0;
            height: 100%;
                font-family: almoni-dl, arial, sans-serif;
    font-weight: 400;
    font-size: 15px;
        }
    `],
    providers: [WillService],
    directives: []
    
})
export class AppComponent {
    
}