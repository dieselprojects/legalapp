// assets/app/work/work.component.ts
import { Component } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: 'my-work',
    templateUrl: 'work.component.html',
    styleUrls:['work.component.css']
})
export class WorkComponent {
}