// assets/app/contact/contact.component.ts
import { Component } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: 'my-contact',
    templateUrl: 'contact.component.html',
    styleUrls:['contact.component.css']
})
export class ContactComponent {
}