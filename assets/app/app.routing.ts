import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import {WillComponent} from "./will/will.component";
import {WILL_ROUTES} from "./will/will.routing";

const APP_ROUTES: Routes = [
    { path: '', component: HomeComponent},
    { path: 'will', component: WillComponent, children: WILL_ROUTES}
];

export const routing = RouterModule.forRoot(APP_ROUTES);