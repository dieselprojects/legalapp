// assets/app/offering/offering.component.ts
import { Component } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: 'my-offering',
    templateUrl: 'offering.component.html',
    styleUrls:['offering.component.css']
})
export class OfferingComponent {
}