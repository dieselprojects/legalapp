"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// assets/app/why/why.component.ts
var core_1 = require("@angular/core");
var WhyComponent = (function () {
    function WhyComponent() {
    }
    WhyComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-why',
            templateUrl: 'why.component.html',
            styleUrls: ['why.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], WhyComponent);
    return WhyComponent;
}());
exports.WhyComponent = WhyComponent;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndoeS93aHkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxrQ0FBa0M7QUFDbEMscUJBQTBCLGVBQWUsQ0FBQyxDQUFBO0FBUTFDO0lBQUE7SUFDQSxDQUFDO0lBUEQ7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFdBQVcsRUFBRSxvQkFBb0I7WUFDakMsU0FBUyxFQUFDLENBQUMsbUJBQW1CLENBQUM7U0FDbEMsQ0FBQzs7b0JBQUE7SUFFRixtQkFBQztBQUFELENBREEsQUFDQyxJQUFBO0FBRFksb0JBQVksZUFDeEIsQ0FBQSIsImZpbGUiOiJ3aHkvd2h5LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGFzc2V0cy9hcHAvd2h5L3doeS5jb21wb25lbnQudHNcclxuaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnbXktd2h5JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnd2h5LmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczpbJ3doeS5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFdoeUNvbXBvbmVudCB7XHJcbn0iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
