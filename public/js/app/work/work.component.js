"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// assets/app/work/work.component.ts
var core_1 = require("@angular/core");
var WorkComponent = (function () {
    function WorkComponent() {
    }
    WorkComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-work',
            templateUrl: 'work.component.html',
            styleUrls: ['work.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], WorkComponent);
    return WorkComponent;
}());
exports.WorkComponent = WorkComponent;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndvcmsvd29yay5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLG9DQUFvQztBQUNwQyxxQkFBMEIsZUFBZSxDQUFDLENBQUE7QUFRMUM7SUFBQTtJQUNBLENBQUM7SUFQRDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLFNBQVM7WUFDbkIsV0FBVyxFQUFFLHFCQUFxQjtZQUNsQyxTQUFTLEVBQUMsQ0FBQyxvQkFBb0IsQ0FBQztTQUNuQyxDQUFDOztxQkFBQTtJQUVGLG9CQUFDO0FBQUQsQ0FEQSxBQUNDLElBQUE7QUFEWSxxQkFBYSxnQkFDekIsQ0FBQSIsImZpbGUiOiJ3b3JrL3dvcmsuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gYXNzZXRzL2FwcC93b3JrL3dvcmsuY29tcG9uZW50LnRzXHJcbmltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ215LXdvcmsnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICd3b3JrLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczpbJ3dvcmsuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBXb3JrQ29tcG9uZW50IHtcclxufSJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
