"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// assets/app/offering/offering.component.ts
var core_1 = require("@angular/core");
var OfferingComponent = (function () {
    function OfferingComponent() {
    }
    OfferingComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-offering',
            templateUrl: 'offering.component.html',
            styleUrls: ['offering.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], OfferingComponent);
    return OfferingComponent;
}());
exports.OfferingComponent = OfferingComponent;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9mZmVyaW5nL29mZmVyaW5nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEsNENBQTRDO0FBQzVDLHFCQUEwQixlQUFlLENBQUMsQ0FBQTtBQVExQztJQUFBO0lBQ0EsQ0FBQztJQVBEO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsYUFBYTtZQUN2QixXQUFXLEVBQUUseUJBQXlCO1lBQ3RDLFNBQVMsRUFBQyxDQUFDLHdCQUF3QixDQUFDO1NBQ3ZDLENBQUM7O3lCQUFBO0lBRUYsd0JBQUM7QUFBRCxDQURBLEFBQ0MsSUFBQTtBQURZLHlCQUFpQixvQkFDN0IsQ0FBQSIsImZpbGUiOiJvZmZlcmluZy9vZmZlcmluZy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBhc3NldHMvYXBwL29mZmVyaW5nL29mZmVyaW5nLmNvbXBvbmVudC50c1xyXG5pbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdteS1vZmZlcmluZycsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ29mZmVyaW5nLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczpbJ29mZmVyaW5nLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgT2ZmZXJpbmdDb21wb25lbnQge1xyXG59Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
