"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// assets/app/aboutus/aboutus.component.ts
var core_1 = require("@angular/core");
var AboutusComponent = (function () {
    function AboutusComponent() {
    }
    AboutusComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-aboutus',
            templateUrl: 'aboutus.component.html',
            styleUrls: ['aboutus.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], AboutusComponent);
    return AboutusComponent;
}());
exports.AboutusComponent = AboutusComponent;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFib3V0dXMvYWJvdXR1cy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLDBDQUEwQztBQUMxQyxxQkFBMEIsZUFBZSxDQUFDLENBQUE7QUFRMUM7SUFBQTtJQUNBLENBQUM7SUFQRDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLFlBQVk7WUFDdEIsV0FBVyxFQUFFLHdCQUF3QjtZQUNyQyxTQUFTLEVBQUMsQ0FBQyx1QkFBdUIsQ0FBQztTQUN0QyxDQUFDOzt3QkFBQTtJQUVGLHVCQUFDO0FBQUQsQ0FEQSxBQUNDLElBQUE7QUFEWSx3QkFBZ0IsbUJBQzVCLENBQUEiLCJmaWxlIjoiYWJvdXR1cy9hYm91dHVzLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGFzc2V0cy9hcHAvYWJvdXR1cy9hYm91dHVzLmNvbXBvbmVudC50c1xyXG5pbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdteS1hYm91dHVzJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnYWJvdXR1cy5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6WydhYm91dHVzLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQWJvdXR1c0NvbXBvbmVudCB7XHJcbn0iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
