"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// assets/app/home/home.component.ts
var core_1 = require("@angular/core");
var http_service_1 = require("./http.service");
var HomeComponent = (function () {
    function HomeComponent(httpService) {
        this.httpService = httpService;
        this.willExists = false;
        this.willObj = null;
    }
    HomeComponent.prototype.onSubmit = function () {
        var _this = this;
        this.httpService.generateWill().subscribe(function (data) {
            _this.willObj = data.json().obj;
            console.log(_this.willObj);
            console.log(_this.willObj.willPdf);
            _this.willPdfStr = _this.willObj.willPdf;
            _this.willExists = true;
            console.log(_this.willExists);
        });
    };
    HomeComponent.prototype.update = function () {
        this.willExists = true;
    };
    HomeComponent.prototype.pdf = function () {
        return this.willPdfStr;
    };
    HomeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-home',
            templateUrl: 'home.component.html',
            styleUrls: ['home.component.css']
        }), 
        __metadata('design:paramtypes', [http_service_1.HttpService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvbWUvaG9tZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLG9DQUFvQztBQUNwQyxxQkFBZ0MsZUFBZSxDQUFDLENBQUE7QUFDaEQsNkJBQTBCLGdCQUFnQixDQUFDLENBQUE7QUFRM0M7SUFNQyx1QkFBb0IsV0FBd0I7UUFBeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDckMsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDeEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFDeEIsQ0FBQztJQUVKLGdDQUFRLEdBQVI7UUFBQSxpQkFXQztRQVZNLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLENBQUMsU0FBUyxDQUNyQyxVQUFDLElBQUk7WUFDRCxLQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUM7WUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2xDLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUM7WUFDdkMsS0FBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDakMsQ0FBQyxDQUNKLENBQUM7SUFDVCxDQUFDO0lBRUUsOEJBQU0sR0FBTjtRQUNJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO0lBQzNCLENBQUM7SUFFRCwyQkFBRyxHQUFIO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDM0IsQ0FBQztJQXBDTDtRQUFDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLFNBQVM7WUFDbkIsV0FBVyxFQUFFLHFCQUFxQjtZQUNsQyxTQUFTLEVBQUMsQ0FBQyxvQkFBb0IsQ0FBQztTQUNoQyxDQUFDOztxQkFBQTtJQWlDRixvQkFBQztBQUFELENBaENBLEFBZ0NDLElBQUE7QUFoQ1kscUJBQWEsZ0JBZ0N6QixDQUFBIiwiZmlsZSI6ImhvbWUvaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBhc3NldHMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQudHNcclxuaW1wb3J0IHtDb21wb25lbnQsIE91dHB1dH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtIdHRwU2VydmljZX0gZnJvbSBcIi4vaHR0cC5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG5cdHNlbGVjdG9yOiAnbXktaG9tZScsXHJcblx0dGVtcGxhdGVVcmw6ICdob21lLmNvbXBvbmVudC5odG1sJyxcclxuXHRzdHlsZVVybHM6Wydob21lLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSG9tZUNvbXBvbmVudCB7XHJcblxyXG4gICAgd2lsbEV4aXN0czogYm9vbGVhbjsgXHJcbiAgICB3aWxsT2JqO1xyXG4gICAgd2lsbFBkZlN0cjogc3RyaW5nO1xyXG4gICAgXHJcblx0Y29uc3RydWN0b3IocHJpdmF0ZSBodHRwU2VydmljZTogSHR0cFNlcnZpY2Upe1xyXG4gICAgICAgIHRoaXMud2lsbEV4aXN0cyA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMud2lsbE9iaiA9IG51bGw7XHJcbiAgICB9XHJcblx0XHJcblx0b25TdWJtaXQoKXtcclxuICAgICAgICB0aGlzLmh0dHBTZXJ2aWNlLmdlbmVyYXRlV2lsbCgpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMud2lsbE9iaiA9IGRhdGEuanNvbigpLm9iajtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMud2lsbE9iaik7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLndpbGxPYmoud2lsbFBkZik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLndpbGxQZGZTdHIgPSB0aGlzLndpbGxPYmoud2lsbFBkZjtcclxuICAgICAgICAgICAgICAgIHRoaXMud2lsbEV4aXN0cyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLndpbGxFeGlzdHMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuXHR9XHJcbiAgICBcclxuICAgIHVwZGF0ZSgpe1xyXG4gICAgICAgIHRoaXMud2lsbEV4aXN0cyA9IHRydWU7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIHBkZigpe1xyXG4gICAgICAgIHJldHVybiB0aGlzLndpbGxQZGZTdHI7XHJcbiAgICB9XHJcblx0XHJcbn0iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
