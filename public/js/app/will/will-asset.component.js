"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var asset_1 = require("./models/asset");
/**
 * Created by idanhahn on 10/20/2016.
 */
var WillAssetComponent = (function () {
    function WillAssetComponent() {
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', asset_1.Asset)
    ], WillAssetComponent.prototype, "asset", void 0);
    WillAssetComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'will-asset',
            templateUrl: 'will-asset.component.html',
            styleUrls: ['will-asset.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], WillAssetComponent);
    return WillAssetComponent;
}());
exports.WillAssetComponent = WillAssetComponent;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvd2lsbC1hc3NldC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUErQixlQUFlLENBQUMsQ0FBQTtBQUMvQyxzQkFBb0IsZ0JBQWdCLENBQUMsQ0FBQTtBQUNyQzs7R0FFRztBQVdIO0lBQUE7SUFPQSxDQUFDO0lBTkc7UUFBQyxZQUFLLEVBQUU7O3FEQUFBO0lBVFo7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxZQUFZO1lBQ3RCLFdBQVcsRUFBRSwyQkFBMkI7WUFDeEMsU0FBUyxFQUFFLENBQUMsMEJBQTBCLENBQUM7U0FDMUMsQ0FBQzs7MEJBQUE7SUFVRix5QkFBQztBQUFELENBUEEsQUFPQyxJQUFBO0FBUFksMEJBQWtCLHFCQU85QixDQUFBIiwiZmlsZSI6IndpbGwvd2lsbC1hc3NldC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7QXNzZXR9IGZyb20gXCIuL21vZGVscy9hc3NldFwiO1xyXG4vKipcclxuICogQ3JlYXRlZCBieSBpZGFuaGFobiBvbiAxMC8yMC8yMDE2LlxyXG4gKi9cclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ3dpbGwtYXNzZXQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICd3aWxsLWFzc2V0LmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWyd3aWxsLWFzc2V0LmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBXaWxsQXNzZXRDb21wb25lbnQge1xyXG4gICAgQElucHV0KCkgYXNzZXQ6IEFzc2V0O1xyXG4gICAgLy9AT3V0cHV0KCkgZWRpdENsaWNrZWQgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcclxuXHJcbiAgICBcclxuXHJcblxyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
