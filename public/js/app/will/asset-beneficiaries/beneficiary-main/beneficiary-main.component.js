/**
 * Created by idanhahn on 10/21/2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var beneficiary_service_1 = require("../beneficiary.service");
var forms_1 = require("@angular/forms");
var beneficiary_1 = require("../../models/beneficiary");
var BeneficiaryMainComponent = (function () {
    function BeneficiaryMainComponent(beneficiaryService) {
        this.beneficiaryService = beneficiaryService;
        // Add Beneficiary:
        // ----------------
        this.addBeneficiary = false;
        // New Beneficiary:
        // ----------------
        this.newBeneficiary = false;
        // Set Beneficiary
        this.setBeneficiary = false;
        // private elements for forms:
        this.genders = [
            'זכר',
            'נקבה'
        ];
        this.form = new forms_1.FormGroup({
            // Basic details:
            'first_name': new forms_1.FormControl(''),
            'last_name': new forms_1.FormControl(''),
            'id': new forms_1.FormControl(''),
            'gender': new forms_1.FormControl(''),
            'birth_date': new forms_1.FormControl(''),
            // address:
            'street': new forms_1.FormControl(''),
            'house': new forms_1.FormControl(''),
            'entry': new forms_1.FormControl(''),
            'apartment': new forms_1.FormControl(''),
            'city': new forms_1.FormControl('')
        });
    }
    BeneficiaryMainComponent.prototype.ngOnInit = function () {
        this.beneficiariesNames = this.beneficiaryService.getBeneficiariesNames();
    };
    // -------------------------- //
    // -- ADD MAIN BENEFICIARY -- //
    // -------------------------- //
    BeneficiaryMainComponent.prototype.addBeneficiaryFunc = function () {
        this.addBeneficiary = true;
    };
    // select main beneficiary or -1 for new beneficiary    
    BeneficiaryMainComponent.prototype.onSelect = function (index) {
        if (index == "-1") {
            this.newBeneficiaryFunc();
        }
        else {
            this.setBeneficiaryFunc(this.beneficiaryService.getBeneficiary(Number(index)));
        }
    };
    // --------------------- //
    // -- New Beneficiary -- //
    // --------------------- //
    BeneficiaryMainComponent.prototype.newBeneficiaryFunc = function () {
        this.setBeneficiary = false;
        this.newBeneficiary = true;
    };
    BeneficiaryMainComponent.prototype.onSubmit = function () {
        console.log('In OnSubmit');
        console.log(this.form);
        this.setBeneficiaryFunc(this.createBeneficiary(this.form));
    };
    BeneficiaryMainComponent.prototype.createBeneficiary = function (form) {
        var beneficiary = new beneficiary_1.Beneficiary();
        //store to DB
        return beneficiary;
    };
    // --------------------- //
    // -- Set beneficiary -- //
    // --------------------- //
    BeneficiaryMainComponent.prototype.setBeneficiaryFunc = function (beneficiary) {
        this.newBeneficiary = false;
        this.displayedBeneficiary = beneficiary;
        this.setBeneficiary = true;
    };
    BeneficiaryMainComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-beneficiary-main',
            templateUrl: 'beneficiary-main.component.html',
            styleUrls: ['beneficiary-main.component.css']
        }), 
        __metadata('design:paramtypes', [beneficiary_service_1.BeneficiaryService])
    ], BeneficiaryMainComponent);
    return BeneficiaryMainComponent;
}());
exports.BeneficiaryMainComponent = BeneficiaryMainComponent;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvYXNzZXQtYmVuZWZpY2lhcmllcy9iZW5lZmljaWFyeS1tYWluL2JlbmVmaWNpYXJ5LW1haW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztHQUVHOzs7Ozs7Ozs7OztBQUVILHFCQUF1QyxlQUFlLENBQUMsQ0FBQTtBQUN2RCxvQ0FBaUMsd0JBQXdCLENBQUMsQ0FBQTtBQUMxRCxzQkFBcUMsZ0JBQWdCLENBQUMsQ0FBQTtBQUN0RCw0QkFBMEIsMEJBQTBCLENBQUMsQ0FBQTtBQVNyRDtJQTBCSSxrQ0FBb0Isa0JBQXNDO1FBQXRDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFyQjFELG1CQUFtQjtRQUNuQixtQkFBbUI7UUFDbkIsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFFaEMsbUJBQW1CO1FBQ25CLG1CQUFtQjtRQUNuQixtQkFBYyxHQUFZLEtBQUssQ0FBQztRQUdoQyxrQkFBa0I7UUFDbEIsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFJaEMsOEJBQThCO1FBQzlCLFlBQU8sR0FBRTtZQUNMLEtBQUs7WUFDTCxNQUFNO1NBQ1QsQ0FBQztRQUtFLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxpQkFBUyxDQUFDO1lBQ3RCLGlCQUFpQjtZQUNqQixZQUFZLEVBQUUsSUFBSSxtQkFBVyxDQUFDLEVBQUUsQ0FBQztZQUNqQyxXQUFXLEVBQUUsSUFBSSxtQkFBVyxDQUFDLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEVBQUUsSUFBSSxtQkFBVyxDQUFDLEVBQUUsQ0FBQztZQUN6QixRQUFRLEVBQUUsSUFBSSxtQkFBVyxDQUFDLEVBQUUsQ0FBQztZQUM3QixZQUFZLEVBQUUsSUFBSSxtQkFBVyxDQUFDLEVBQUUsQ0FBQztZQUNqQyxXQUFXO1lBQ1gsUUFBUSxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7WUFDN0IsT0FBTyxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7WUFDNUIsT0FBTyxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7WUFDNUIsV0FBVyxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7WUFDaEMsTUFBTSxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7U0FFOUIsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVELDJDQUFRLEdBQVI7UUFFSSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixFQUFFLENBQUM7SUFFOUUsQ0FBQztJQUVELGdDQUFnQztJQUNoQyxnQ0FBZ0M7SUFDaEMsZ0NBQWdDO0lBRWhDLHFEQUFrQixHQUFsQjtRQUNJLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO0lBQy9CLENBQUM7SUFFRCx3REFBd0Q7SUFDeEQsMkNBQVEsR0FBUixVQUFTLEtBQWE7UUFFbEIsRUFBRSxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDZixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM5QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ2xGLENBQUM7SUFFTCxDQUFDO0lBRUQsMkJBQTJCO0lBQzNCLDJCQUEyQjtJQUMzQiwyQkFBMkI7SUFFM0IscURBQWtCLEdBQWxCO1FBQ0ksSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7UUFDNUIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7SUFDL0IsQ0FBQztJQUVELDJDQUFRLEdBQVI7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBS3ZCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFFL0QsQ0FBQztJQUdELG9EQUFpQixHQUFqQixVQUFrQixJQUFlO1FBQzdCLElBQUksV0FBVyxHQUFnQixJQUFJLHlCQUFXLEVBQUUsQ0FBQztRQUVqRCxhQUFhO1FBRWIsTUFBTSxDQUFDLFdBQVcsQ0FBQztJQUN2QixDQUFDO0lBSUQsMkJBQTJCO0lBQzNCLDJCQUEyQjtJQUMzQiwyQkFBMkI7SUFFM0IscURBQWtCLEdBQWxCLFVBQW1CLFdBQXdCO1FBRXZDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxXQUFXLENBQUM7UUFDeEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7SUFFL0IsQ0FBQztJQXRITDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLHNCQUFzQjtZQUNoQyxXQUFXLEVBQUUsaUNBQWlDO1lBQzlDLFNBQVMsRUFBRSxDQUFDLGdDQUFnQyxDQUFDO1NBQ2hELENBQUM7O2dDQUFBO0lBb0hGLCtCQUFDO0FBQUQsQ0FsSEEsQUFrSEMsSUFBQTtBQWxIWSxnQ0FBd0IsMkJBa0hwQyxDQUFBIiwiZmlsZSI6IndpbGwvYXNzZXQtYmVuZWZpY2lhcmllcy9iZW5lZmljaWFyeS1tYWluL2JlbmVmaWNpYXJ5LW1haW4uY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiAqIENyZWF0ZWQgYnkgaWRhbmhhaG4gb24gMTAvMjEvMjAxNi5cclxuICovXHJcblxyXG5pbXBvcnQge0NvbXBvbmVudCwgT25Jbml0LCBJbnB1dH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtCZW5lZmljaWFyeVNlcnZpY2V9IGZyb20gXCIuLi9iZW5lZmljaWFyeS5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7Rm9ybUdyb3VwLCBGb3JtQ29udHJvbH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcbmltcG9ydCB7QmVuZWZpY2lhcnl9IGZyb20gXCIuLi8uLi9tb2RlbHMvYmVuZWZpY2lhcnlcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnYXBwLWJlbmVmaWNpYXJ5LW1haW4nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdiZW5lZmljaWFyeS1tYWluLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWydiZW5lZmljaWFyeS1tYWluLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEJlbmVmaWNpYXJ5TWFpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdHtcclxuICBcclxuICAgIC8vIGhvbGRzIGFsbCBiZW5lZmljaWFyaWVzOlxyXG4gICAgYmVuZWZpY2lhcmllc05hbWVzOiBBcnJheTxzdHJpbmc+O1xyXG4gICAgXHJcbiAgICAvLyBBZGQgQmVuZWZpY2lhcnk6XHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tXHJcbiAgICBhZGRCZW5lZmljaWFyeTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgXHJcbiAgICAvLyBOZXcgQmVuZWZpY2lhcnk6XHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tXHJcbiAgICBuZXdCZW5lZmljaWFyeTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgZm9ybTogRm9ybUdyb3VwO1xyXG4gICAgXHJcbiAgICAvLyBTZXQgQmVuZWZpY2lhcnlcclxuICAgIHNldEJlbmVmaWNpYXJ5OiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBkaXNwbGF5ZWRCZW5lZmljaWFyeTogQmVuZWZpY2lhcnk7XHJcbiAgICBcclxuICAgIFxyXG4gICAgLy8gcHJpdmF0ZSBlbGVtZW50cyBmb3IgZm9ybXM6XHJcbiAgICBnZW5kZXJzID1bXHJcbiAgICAgICAgJ9eW15vXqCcsXHJcbiAgICAgICAgJ9eg16fXkdeUJ1xyXG4gICAgXTtcclxuICAgIFxyXG4gICAgXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGJlbmVmaWNpYXJ5U2VydmljZTogQmVuZWZpY2lhcnlTZXJ2aWNlKXtcclxuXHJcbiAgICAgICAgdGhpcy5mb3JtID0gbmV3IEZvcm1Hcm91cCh7XHJcbiAgICAgICAgICAgIC8vIEJhc2ljIGRldGFpbHM6XHJcbiAgICAgICAgICAgICdmaXJzdF9uYW1lJzogbmV3IEZvcm1Db250cm9sKCcnKSxcclxuICAgICAgICAgICAgJ2xhc3RfbmFtZSc6IG5ldyBGb3JtQ29udHJvbCgnJyksXHJcbiAgICAgICAgICAgICdpZCc6IG5ldyBGb3JtQ29udHJvbCgnJyksXHJcbiAgICAgICAgICAgICdnZW5kZXInOiBuZXcgRm9ybUNvbnRyb2woJycpLFxyXG4gICAgICAgICAgICAnYmlydGhfZGF0ZSc6IG5ldyBGb3JtQ29udHJvbCgnJyksXHJcbiAgICAgICAgICAgIC8vIGFkZHJlc3M6XHJcbiAgICAgICAgICAgICdzdHJlZXQnOiBuZXcgRm9ybUNvbnRyb2woJycpLFxyXG4gICAgICAgICAgICAnaG91c2UnOiBuZXcgRm9ybUNvbnRyb2woJycpLFxyXG4gICAgICAgICAgICAnZW50cnknOiBuZXcgRm9ybUNvbnRyb2woJycpLFxyXG4gICAgICAgICAgICAnYXBhcnRtZW50JzogbmV3IEZvcm1Db250cm9sKCcnKSxcclxuICAgICAgICAgICAgJ2NpdHknOiBuZXcgRm9ybUNvbnRyb2woJycpXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcbiAgICBcclxuICAgIG5nT25Jbml0KCk6dm9pZCB7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5iZW5lZmljaWFyaWVzTmFtZXMgPSB0aGlzLmJlbmVmaWNpYXJ5U2VydmljZS5nZXRCZW5lZmljaWFyaWVzTmFtZXMoKTtcclxuICAgICAgICBcclxuICAgIH1cclxuICAgIFxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gLy9cclxuICAgIC8vIC0tIEFERCBNQUlOIEJFTkVGSUNJQVJZIC0tIC8vXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSAvL1xyXG4gICAgXHJcbiAgICBhZGRCZW5lZmljaWFyeUZ1bmMoKXtcclxuICAgICAgICB0aGlzLmFkZEJlbmVmaWNpYXJ5ID0gdHJ1ZTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLy8gc2VsZWN0IG1haW4gYmVuZWZpY2lhcnkgb3IgLTEgZm9yIG5ldyBiZW5lZmljaWFyeSAgICBcclxuICAgIG9uU2VsZWN0KGluZGV4OiBzdHJpbmcpe1xyXG4gICAgICAgXHJcbiAgICAgICAgaWYgKGluZGV4ID09IFwiLTFcIil7XHJcbiAgICAgICAgICAgIHRoaXMubmV3QmVuZWZpY2lhcnlGdW5jKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRCZW5lZmljaWFyeUZ1bmModGhpcy5iZW5lZmljaWFyeVNlcnZpY2UuZ2V0QmVuZWZpY2lhcnkoTnVtYmVyKGluZGV4KSkpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgfVxyXG4gICAgXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0gLy9cclxuICAgIC8vIC0tIE5ldyBCZW5lZmljaWFyeSAtLSAvL1xyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tIC8vXHJcbiAgICBcclxuICAgIG5ld0JlbmVmaWNpYXJ5RnVuYygpe1xyXG4gICAgICAgIHRoaXMuc2V0QmVuZWZpY2lhcnkgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLm5ld0JlbmVmaWNpYXJ5ID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBvblN1Ym1pdCgpe1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdJbiBPblN1Ym1pdCcpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuZm9ybSk7XHJcblxyXG4gICAgICAgIFxyXG5cclxuICAgICAgICBcclxuICAgICAgICB0aGlzLnNldEJlbmVmaWNpYXJ5RnVuYyh0aGlzLmNyZWF0ZUJlbmVmaWNpYXJ5KHRoaXMuZm9ybSkpO1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBcclxuICAgIGNyZWF0ZUJlbmVmaWNpYXJ5KGZvcm06IEZvcm1Hcm91cCk6IEJlbmVmaWNpYXJ5e1xyXG4gICAgICAgIGxldCBiZW5lZmljaWFyeTogQmVuZWZpY2lhcnkgPSBuZXcgQmVuZWZpY2lhcnkoKTtcclxuICAgICAgIFxyXG4gICAgICAgIC8vc3RvcmUgdG8gREJcclxuICAgICAgICBcclxuICAgICAgICByZXR1cm4gYmVuZWZpY2lhcnk7IFxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBcclxuICAgIFxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tIC8vXHJcbiAgICAvLyAtLSBTZXQgYmVuZWZpY2lhcnkgLS0gLy9cclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLSAvL1xyXG4gICAgXHJcbiAgICBzZXRCZW5lZmljaWFyeUZ1bmMoYmVuZWZpY2lhcnk6IEJlbmVmaWNpYXJ5KXtcclxuICAgICAgIFxyXG4gICAgICAgIHRoaXMubmV3QmVuZWZpY2lhcnkgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmRpc3BsYXllZEJlbmVmaWNpYXJ5ID0gYmVuZWZpY2lhcnk7XHJcbiAgICAgICAgdGhpcy5zZXRCZW5lZmljaWFyeSA9IHRydWU7XHJcbiAgICAgICAgXHJcbiAgICB9XHJcbiAgICBcclxuICAgIFxyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
