"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var beneficiary_1 = require("../../models/beneficiary");
/**
 * Created by idanhahn on 10/21/2016.
 */
var BeneficiaryComponent = (function () {
    function BeneficiaryComponent() {
    }
    BeneficiaryComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', beneficiary_1.Beneficiary)
    ], BeneficiaryComponent.prototype, "beneficiary", void 0);
    BeneficiaryComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-beneficiary',
            templateUrl: 'beneficiary.component.html',
            styleUrls: ['beneficiary.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], BeneficiaryComponent);
    return BeneficiaryComponent;
}());
exports.BeneficiaryComponent = BeneficiaryComponent;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvYXNzZXQtYmVuZWZpY2lhcmllcy9iZW5lZmljaWFyeS9iZW5lZmljaWFyeS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF1QyxlQUFlLENBQUMsQ0FBQTtBQUN2RCw0QkFBMEIsMEJBQTBCLENBQUMsQ0FBQTtBQUNyRDs7R0FFRztBQVFIO0lBQUE7SUFNQSxDQUFDO0lBSEcsdUNBQVEsR0FBUjtJQUNBLENBQUM7SUFIRDtRQUFDLFlBQUssRUFBRTs7NkRBQUE7SUFQWjtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixXQUFXLEVBQUUsNEJBQTRCO1lBQ3pDLFNBQVMsRUFBRSxDQUFDLDJCQUEyQixDQUFDO1NBQzNDLENBQUM7OzRCQUFBO0lBT0YsMkJBQUM7QUFBRCxDQU5BLEFBTUMsSUFBQTtBQU5ZLDRCQUFvQix1QkFNaEMsQ0FBQSIsImZpbGUiOiJ3aWxsL2Fzc2V0LWJlbmVmaWNpYXJpZXMvYmVuZWZpY2lhcnkvYmVuZWZpY2lhcnkuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdCwgSW5wdXR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7QmVuZWZpY2lhcnl9IGZyb20gXCIuLi8uLi9tb2RlbHMvYmVuZWZpY2lhcnlcIjtcclxuLyoqXHJcbiAqIENyZWF0ZWQgYnkgaWRhbmhhaG4gb24gMTAvMjEvMjAxNi5cclxuICovXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ2FwcC1iZW5lZmljaWFyeScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ2JlbmVmaWNpYXJ5LmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWydiZW5lZmljaWFyeS5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEJlbmVmaWNpYXJ5Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIEBJbnB1dCgpIGJlbmVmaWNpYXJ5OiBCZW5lZmljaWFyeTtcclxuICAgIFxyXG4gICAgbmdPbkluaXQoKTp2b2lkIHtcclxuICAgIH1cclxuICAgIFxyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
