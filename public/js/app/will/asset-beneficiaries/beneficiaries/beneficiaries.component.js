"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
/**
 * Created by idanhahn on 10/21/2016.
 */
var BeneficiariesComponent = (function () {
    function BeneficiariesComponent() {
    }
    BeneficiariesComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-beneficiaries',
            templateUrl: 'beneficiaries.component.html',
            styleUrls: ['beneficiaries.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], BeneficiariesComponent);
    return BeneficiariesComponent;
}());
exports.BeneficiariesComponent = BeneficiariesComponent;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvYXNzZXQtYmVuZWZpY2lhcmllcy9iZW5lZmljaWFyaWVzL2JlbmVmaWNpYXJpZXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBd0IsZUFBZSxDQUFDLENBQUE7QUFDeEM7O0dBRUc7QUFRSDtJQUFBO0lBRUEsQ0FBQztJQVJEO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLFdBQVcsRUFBRSw4QkFBOEI7WUFDM0MsU0FBUyxFQUFDLENBQUMsNkJBQTZCLENBQUM7U0FDNUMsQ0FBQzs7OEJBQUE7SUFHRiw2QkFBQztBQUFELENBRkEsQUFFQyxJQUFBO0FBRlksOEJBQXNCLHlCQUVsQyxDQUFBIiwiZmlsZSI6IndpbGwvYXNzZXQtYmVuZWZpY2lhcmllcy9iZW5lZmljaWFyaWVzL2JlbmVmaWNpYXJpZXMuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbi8qKlxyXG4gKiBDcmVhdGVkIGJ5IGlkYW5oYWhuIG9uIDEwLzIxLzIwMTYuXHJcbiAqL1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdhcHAtYmVuZWZpY2lhcmllcycsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ2JlbmVmaWNpYXJpZXMuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOlsnYmVuZWZpY2lhcmllcy5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEJlbmVmaWNpYXJpZXNDb21wb25lbnQge1xyXG4gICAgXHJcbn0iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
