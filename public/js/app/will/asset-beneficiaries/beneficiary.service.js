"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var beneficiary_1 = require("../models/beneficiary");
var address_1 = require("../models/address");
/**
 * Created by idanhahn on 10/21/2016.
 */
var BeneficiaryService = (function () {
    function BeneficiaryService() {
        this.beneficiaries = {};
        this.createNew();
    }
    // (create new for debug only)
    BeneficiaryService.prototype.createNew = function () {
        var address = new address_1.Address();
        address.block_type = "address_0";
        address.street = "דרך השלום";
        address.house = "81";
        address.entry = "1";
        address.apartment = "ב";
        address.city = "תל אביב, יפו";
        var beneficiary = new beneficiary_1.Beneficiary();
        beneficiary.block_type = "beneficiary_main_female_0";
        beneficiary.first_name = "מורן";
        beneficiary.last_name = "פוסק";
        beneficiary.id = "66554654654";
        beneficiary.birth_date = new Date();
        beneficiary.relation = "אישה";
        beneficiary.address = address;
        var address2 = new address_1.Address();
        address.block_type = "address_0";
        address.street = "דרך השלום";
        address.house = "81";
        address.entry = "1";
        address.apartment = "ב";
        address.city = "תל אביב, יפו";
        var beneficiary2 = new beneficiary_1.Beneficiary();
        beneficiary2.block_type = "beneficiary_main_male_0";
        beneficiary2.first_name = "עידן";
        beneficiary2.last_name = "האן";
        beneficiary2.id = "037493509";
        beneficiary2.birth_date = new Date();
        beneficiary2.relation = "אח";
        beneficiary2.address = address2;
        this.beneficiaries[beneficiary.getFullName()] = beneficiary;
        this.beneficiaries[beneficiary2.getFullName()] = beneficiary2;
    };
    BeneficiaryService.prototype.getBeneficiaries = function () {
        return this.beneficiaries;
    };
    BeneficiaryService.prototype.addBeneficiary = function (key, beneficiary) {
        this.beneficiaries[key] = beneficiary;
    };
    BeneficiaryService.prototype.getBeneficiariesNames = function () {
        var names = [];
        for (var beneficiary in this.beneficiaries) {
            names.push(beneficiary);
        }
        return names;
    };
    BeneficiaryService.prototype.getBeneficiary = function (index) {
        var names = [];
        for (var beneficiary in this.beneficiaries) {
            names.push(beneficiary);
        }
        return this.beneficiaries[names[index]];
    };
    BeneficiaryService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], BeneficiaryService);
    return BeneficiaryService;
}());
exports.BeneficiaryService = BeneficiaryService;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvYXNzZXQtYmVuZWZpY2lhcmllcy9iZW5lZmljaWFyeS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsNEJBQTBCLHVCQUF1QixDQUFDLENBQUE7QUFDbEQsd0JBQXNCLG1CQUFtQixDQUFDLENBQUE7QUFDMUM7O0dBRUc7QUFHSDtJQThDSTtRQTVDUSxrQkFBYSxHQUErQixFQUFFLENBQUM7UUE2Q25ELElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBNUNELDhCQUE4QjtJQUM5QixzQ0FBUyxHQUFUO1FBRUksSUFBSSxPQUFPLEdBQUcsSUFBSSxpQkFBTyxFQUFFLENBQUM7UUFDNUIsT0FBTyxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUM7UUFDakMsT0FBTyxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUM7UUFDN0IsT0FBTyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDckIsT0FBTyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDcEIsT0FBTyxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUM7UUFDeEIsT0FBTyxDQUFDLElBQUksR0FBRyxjQUFjLENBQUM7UUFFOUIsSUFBSSxXQUFXLEdBQUcsSUFBSSx5QkFBVyxFQUFFLENBQUM7UUFDcEMsV0FBVyxDQUFDLFVBQVUsR0FBRywyQkFBMkIsQ0FBQztRQUNyRCxXQUFXLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQztRQUNoQyxXQUFXLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQztRQUMvQixXQUFXLENBQUMsRUFBRSxHQUFHLGFBQWEsQ0FBQztRQUMvQixXQUFXLENBQUMsVUFBVSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDcEMsV0FBVyxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUM7UUFDOUIsV0FBVyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFHOUIsSUFBSSxRQUFRLEdBQUcsSUFBSSxpQkFBTyxFQUFFLENBQUM7UUFDN0IsT0FBTyxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUM7UUFDakMsT0FBTyxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUM7UUFDN0IsT0FBTyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDckIsT0FBTyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDcEIsT0FBTyxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUM7UUFDeEIsT0FBTyxDQUFDLElBQUksR0FBRyxjQUFjLENBQUM7UUFFOUIsSUFBSSxZQUFZLEdBQUcsSUFBSSx5QkFBVyxFQUFFLENBQUM7UUFDckMsWUFBWSxDQUFDLFVBQVUsR0FBRyx5QkFBeUIsQ0FBQztRQUNwRCxZQUFZLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQztRQUNqQyxZQUFZLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUMvQixZQUFZLENBQUMsRUFBRSxHQUFHLFdBQVcsQ0FBQztRQUM5QixZQUFZLENBQUMsVUFBVSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDckMsWUFBWSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDN0IsWUFBWSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUM7UUFFaEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxXQUFXLENBQUM7UUFDNUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxZQUFZLENBQUM7SUFDbEUsQ0FBQztJQU9ELDZDQUFnQixHQUFoQjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzlCLENBQUM7SUFFRCwyQ0FBYyxHQUFkLFVBQWUsR0FBVyxFQUFFLFdBQXVCO1FBRS9DLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLEdBQUcsV0FBVyxDQUFDO0lBRTFDLENBQUM7SUFFRCxrREFBcUIsR0FBckI7UUFFSSxJQUFJLEtBQUssR0FBa0IsRUFBRSxDQUFDO1FBRTlCLEdBQUcsQ0FBQyxDQUFDLElBQUksV0FBVyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQSxDQUFDO1lBQ3hDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDNUIsQ0FBQztRQUVELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFFakIsQ0FBQztJQUVELDJDQUFjLEdBQWQsVUFBZSxLQUFhO1FBRXhCLElBQUksS0FBSyxHQUFrQixFQUFFLENBQUM7UUFFOUIsR0FBRyxDQUFDLENBQUMsSUFBSSxXQUFXLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBLENBQUM7WUFDeEMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM1QixDQUFDO1FBRUQsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFFNUMsQ0FBQztJQXBGTDtRQUFDLGlCQUFVLEVBQUU7OzBCQUFBO0lBc0ZiLHlCQUFDO0FBQUQsQ0FyRkEsQUFxRkMsSUFBQTtBQXJGWSwwQkFBa0IscUJBcUY5QixDQUFBIiwiZmlsZSI6IndpbGwvYXNzZXQtYmVuZWZpY2lhcmllcy9iZW5lZmljaWFyeS5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge0JlbmVmaWNpYXJ5fSBmcm9tIFwiLi4vbW9kZWxzL2JlbmVmaWNpYXJ5XCI7XHJcbmltcG9ydCB7QWRkcmVzc30gZnJvbSBcIi4uL21vZGVscy9hZGRyZXNzXCI7XHJcbi8qKlxyXG4gKiBDcmVhdGVkIGJ5IGlkYW5oYWhuIG9uIDEwLzIxLzIwMTYuXHJcbiAqL1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQmVuZWZpY2lhcnlTZXJ2aWNlIHtcclxuXHJcbiAgICBwcml2YXRlIGJlbmVmaWNpYXJpZXM6eyBba2V5OnN0cmluZ106QmVuZWZpY2lhcnl9ID0ge307XHJcbiAgICBcclxuICAgIC8vIChjcmVhdGUgbmV3IGZvciBkZWJ1ZyBvbmx5KVxyXG4gICAgY3JlYXRlTmV3KCl7XHJcbiAgICAgICAgXHJcbiAgICAgICAgbGV0IGFkZHJlc3MgPSBuZXcgQWRkcmVzcygpO1xyXG4gICAgICAgIGFkZHJlc3MuYmxvY2tfdHlwZSA9IFwiYWRkcmVzc18wXCI7XHJcbiAgICAgICAgYWRkcmVzcy5zdHJlZXQgPSBcIteT16jXmiDXlNep15zXldedXCI7XHJcbiAgICAgICAgYWRkcmVzcy5ob3VzZSA9IFwiODFcIjtcclxuICAgICAgICBhZGRyZXNzLmVudHJ5ID0gXCIxXCI7XHJcbiAgICAgICAgYWRkcmVzcy5hcGFydG1lbnQgPSBcIteRXCI7XHJcbiAgICAgICAgYWRkcmVzcy5jaXR5ID0gXCLXqtecINeQ15HXmdeRLCDXmdek15VcIjtcclxuICAgICAgICBcclxuICAgICAgICBsZXQgYmVuZWZpY2lhcnkgPSBuZXcgQmVuZWZpY2lhcnkoKTtcclxuICAgICAgICBiZW5lZmljaWFyeS5ibG9ja190eXBlID0gXCJiZW5lZmljaWFyeV9tYWluX2ZlbWFsZV8wXCI7XHJcbiAgICAgICAgYmVuZWZpY2lhcnkuZmlyc3RfbmFtZSA9IFwi157Xldeo159cIjtcclxuICAgICAgICBiZW5lZmljaWFyeS5sYXN0X25hbWUgPSBcItek15XXodenXCI7XHJcbiAgICAgICAgYmVuZWZpY2lhcnkuaWQgPSBcIjY2NTU0NjU0NjU0XCI7XHJcbiAgICAgICAgYmVuZWZpY2lhcnkuYmlydGhfZGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgYmVuZWZpY2lhcnkucmVsYXRpb24gPSBcIteQ15nXqdeUXCI7XHJcbiAgICAgICAgYmVuZWZpY2lhcnkuYWRkcmVzcyA9IGFkZHJlc3M7XHJcblxyXG5cclxuICAgICAgICBsZXQgYWRkcmVzczIgPSBuZXcgQWRkcmVzcygpO1xyXG4gICAgICAgIGFkZHJlc3MuYmxvY2tfdHlwZSA9IFwiYWRkcmVzc18wXCI7XHJcbiAgICAgICAgYWRkcmVzcy5zdHJlZXQgPSBcIteT16jXmiDXlNep15zXldedXCI7XHJcbiAgICAgICAgYWRkcmVzcy5ob3VzZSA9IFwiODFcIjtcclxuICAgICAgICBhZGRyZXNzLmVudHJ5ID0gXCIxXCI7XHJcbiAgICAgICAgYWRkcmVzcy5hcGFydG1lbnQgPSBcIteRXCI7XHJcbiAgICAgICAgYWRkcmVzcy5jaXR5ID0gXCLXqtecINeQ15HXmdeRLCDXmdek15VcIjtcclxuXHJcbiAgICAgICAgbGV0IGJlbmVmaWNpYXJ5MiA9IG5ldyBCZW5lZmljaWFyeSgpO1xyXG4gICAgICAgIGJlbmVmaWNpYXJ5Mi5ibG9ja190eXBlID0gXCJiZW5lZmljaWFyeV9tYWluX21hbGVfMFwiO1xyXG4gICAgICAgIGJlbmVmaWNpYXJ5Mi5maXJzdF9uYW1lID0gXCLXoteZ15PXn1wiO1xyXG4gICAgICAgIGJlbmVmaWNpYXJ5Mi5sYXN0X25hbWUgPSBcIteU15DXn1wiO1xyXG4gICAgICAgIGJlbmVmaWNpYXJ5Mi5pZCA9IFwiMDM3NDkzNTA5XCI7XHJcbiAgICAgICAgYmVuZWZpY2lhcnkyLmJpcnRoX2RhdGUgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgIGJlbmVmaWNpYXJ5Mi5yZWxhdGlvbiA9IFwi15DXl1wiO1xyXG4gICAgICAgIGJlbmVmaWNpYXJ5Mi5hZGRyZXNzID0gYWRkcmVzczI7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5iZW5lZmljaWFyaWVzW2JlbmVmaWNpYXJ5LmdldEZ1bGxOYW1lKCldID0gYmVuZWZpY2lhcnk7XHJcbiAgICAgICAgdGhpcy5iZW5lZmljaWFyaWVzW2JlbmVmaWNpYXJ5Mi5nZXRGdWxsTmFtZSgpXSA9IGJlbmVmaWNpYXJ5MjtcclxuICAgIH1cclxuICAgIFxyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICB0aGlzLmNyZWF0ZU5ldygpO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBcclxuICAgIGdldEJlbmVmaWNpYXJpZXMoKTphbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmJlbmVmaWNpYXJpZXM7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkQmVuZWZpY2lhcnkoa2V5OiBzdHJpbmcsIGJlbmVmaWNpYXJ5OkJlbmVmaWNpYXJ5KSB7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5iZW5lZmljaWFyaWVzW2tleV0gPSBiZW5lZmljaWFyeTtcclxuICAgICAgICBcclxuICAgIH1cclxuICAgIFxyXG4gICAgZ2V0QmVuZWZpY2lhcmllc05hbWVzKCk6IEFycmF5PHN0cmluZz57XHJcbiAgICAgIFxyXG4gICAgICAgIGxldCBuYW1lczogQXJyYXk8c3RyaW5nPiA9IFtdO1xyXG5cclxuICAgICAgICBmb3IgKGxldCBiZW5lZmljaWFyeSBpbiB0aGlzLmJlbmVmaWNpYXJpZXMpe1xyXG4gICAgICAgICAgICBuYW1lcy5wdXNoKGJlbmVmaWNpYXJ5KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgcmV0dXJuIG5hbWVzO1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG4gICBcclxuICAgIGdldEJlbmVmaWNpYXJ5KGluZGV4OiBudW1iZXIpe1xyXG5cclxuICAgICAgICBsZXQgbmFtZXM6IEFycmF5PHN0cmluZz4gPSBbXTtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgYmVuZWZpY2lhcnkgaW4gdGhpcy5iZW5lZmljaWFyaWVzKXtcclxuICAgICAgICAgICAgbmFtZXMucHVzaChiZW5lZmljaWFyeSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIHJldHVybiB0aGlzLmJlbmVmaWNpYXJpZXNbbmFtZXNbaW5kZXhdXTtcclxuICAgICAgICBcclxuICAgIH1cclxuICAgIFxyXG59Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
