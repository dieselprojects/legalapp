"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
/**
 * Created by idanhahn on 10/4/2016.
 */
var WillService = (function () {
    function WillService() {
        // key is section type
        this.sections = {};
    }
    // --------------------- //
    // -- Handle Testator -- //
    // --------------------- //
    /*
        addWill0Form(will0FormObj:Will0FormObject) {
            console.log(will0FormObj);
            this.user.firstName = will0FormObj.userFirstName;
        }
    */
    // --------------------- //
    // -- Handle sections -- //
    // --------------------- //
    WillService.prototype.addSection = function (key, section) {
        this.sections[key] = section;
        console.log("WillService: Add Section " + key);
        console.log("WillService: Add Section(Array status) ", this.sections);
    };
    WillService.prototype.getSection = function (key) {
        console.log("WillService: Get Section " + key);
        console.log("WillService: Get Section(result) ", this.sections[key]);
        console.log("WillService: Get Section(Array status) ", this.sections);
        if (key in this.sections) {
            return this.sections[key];
        }
        else {
            console.error("WillService: Get Section Failed with key " + key);
            console.error(this.sections);
            return null;
        }
    };
    WillService.prototype.deleteSection = function (key) {
        console.error("Implement WillService delete");
    };
    WillService.prototype.updateSection = function (key, section) {
        // check if exists
        if (key in this.sections) {
            this.sections[key] = section;
        }
        else {
            console.error("WillService: Update Section Failed with key " + key);
            console.error(this.sections);
            return;
        }
        console.log("WillService: Update Section " + key);
        console.log("WillService: Update Section(result) ", this.sections[key]);
        console.log("WillService: Update Section(Array status) ", this.sections);
    };
    // Debug helpers:
    WillService.prototype.printSections = function () {
        console.log(this.sections);
    };
    WillService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], WillService);
    return WillService;
}());
exports.WillService = WillService;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvd2lsbC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFJekM7O0dBRUc7QUFHSDtJQU1JO1FBSkEsc0JBQXNCO1FBQ2QsYUFBUSxHQUE0QixFQUFFLENBQUM7SUFJL0MsQ0FBQztJQUdELDJCQUEyQjtJQUMzQiwyQkFBMkI7SUFDM0IsMkJBQTJCO0lBQy9COzs7OztNQUtFO0lBQ0UsMkJBQTJCO0lBQzNCLDJCQUEyQjtJQUMzQiwyQkFBMkI7SUFFM0IsZ0NBQVUsR0FBVixVQUFXLEdBQVUsRUFBRSxPQUFlO1FBRWxDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsT0FBTyxDQUFDO1FBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5Q0FBeUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFFMUUsQ0FBQztJQUVELGdDQUFVLEdBQVYsVUFBVyxHQUFVO1FBRWpCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQ0FBbUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDckUsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5Q0FBeUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFdEUsRUFBRSxDQUFDLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzlCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE9BQU8sQ0FBQyxLQUFLLENBQUMsMkNBQTJDLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDakUsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDN0IsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNoQixDQUFDO0lBRUwsQ0FBQztJQUVELG1DQUFhLEdBQWIsVUFBYyxHQUFVO1FBQ3BCLE9BQU8sQ0FBQyxLQUFLLENBQUMsOEJBQThCLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRUQsbUNBQWEsR0FBYixVQUFjLEdBQVUsRUFBRSxPQUFlO1FBRXJDLGtCQUFrQjtRQUVsQixFQUFFLENBQUMsQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxPQUFPLENBQUM7UUFDakMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osT0FBTyxDQUFDLEtBQUssQ0FBQyw4Q0FBOEMsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNwRSxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM3QixNQUFNLENBQUM7UUFDWCxDQUFDO1FBRUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsR0FBRyxHQUFHLENBQUMsQ0FBQztRQUNsRCxPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN4RSxPQUFPLENBQUMsR0FBRyxDQUFDLDRDQUE0QyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUU3RSxDQUFDO0lBRUQsaUJBQWlCO0lBQ2pCLG1DQUFhLEdBQWI7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBekVMO1FBQUMsaUJBQVUsRUFBRTs7bUJBQUE7SUEyRWIsa0JBQUM7QUFBRCxDQTFFQSxBQTBFQyxJQUFBO0FBMUVZLG1CQUFXLGNBMEV2QixDQUFBIiwiZmlsZSI6IndpbGwvd2lsbC5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge1NlY3Rpb259IGZyb20gXCIuL21vZGVscy9zZWN0aW9uXCI7XHJcbmltcG9ydCB7QmVuZWZpY2lhcnl9IGZyb20gXCIuL21vZGVscy9iZW5lZmljaWFyeVwiO1xyXG5cclxuLyoqXHJcbiAqIENyZWF0ZWQgYnkgaWRhbmhhaG4gb24gMTAvNC8yMDE2LlxyXG4gKi9cclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFdpbGxTZXJ2aWNlIHtcclxuXHJcbiAgICAvLyBrZXkgaXMgc2VjdGlvbiB0eXBlXHJcbiAgICBwcml2YXRlIHNlY3Rpb25zOnsgW2tleTpzdHJpbmddOlNlY3Rpb24gfSA9IHt9O1xyXG5cclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIH1cclxuXHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tIC8vXHJcbiAgICAvLyAtLSBIYW5kbGUgVGVzdGF0b3IgLS0gLy9cclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLSAvL1xyXG4vKlxyXG4gICAgYWRkV2lsbDBGb3JtKHdpbGwwRm9ybU9iajpXaWxsMEZvcm1PYmplY3QpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyh3aWxsMEZvcm1PYmopO1xyXG4gICAgICAgIHRoaXMudXNlci5maXJzdE5hbWUgPSB3aWxsMEZvcm1PYmoudXNlckZpcnN0TmFtZTtcclxuICAgIH1cclxuKi9cclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLSAvL1xyXG4gICAgLy8gLS0gSGFuZGxlIHNlY3Rpb25zIC0tIC8vXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0gLy9cclxuXHJcbiAgICBhZGRTZWN0aW9uKGtleTpzdHJpbmcsIHNlY3Rpb246U2VjdGlvbikge1xyXG5cclxuICAgICAgICB0aGlzLnNlY3Rpb25zW2tleV0gPSBzZWN0aW9uO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiV2lsbFNlcnZpY2U6IEFkZCBTZWN0aW9uIFwiICsga2V5KTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIldpbGxTZXJ2aWNlOiBBZGQgU2VjdGlvbihBcnJheSBzdGF0dXMpIFwiLCB0aGlzLnNlY3Rpb25zKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U2VjdGlvbihrZXk6c3RyaW5nKSB7XHJcblxyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiV2lsbFNlcnZpY2U6IEdldCBTZWN0aW9uIFwiICsga2V5KTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIldpbGxTZXJ2aWNlOiBHZXQgU2VjdGlvbihyZXN1bHQpIFwiLCB0aGlzLnNlY3Rpb25zW2tleV0pO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiV2lsbFNlcnZpY2U6IEdldCBTZWN0aW9uKEFycmF5IHN0YXR1cykgXCIsIHRoaXMuc2VjdGlvbnMpO1xyXG5cclxuICAgICAgICBpZiAoa2V5IGluIHRoaXMuc2VjdGlvbnMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuc2VjdGlvbnNba2V5XTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiV2lsbFNlcnZpY2U6IEdldCBTZWN0aW9uIEZhaWxlZCB3aXRoIGtleSBcIiArIGtleSk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IodGhpcy5zZWN0aW9ucyk7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG4gICAgZGVsZXRlU2VjdGlvbihrZXk6c3RyaW5nKSB7XHJcbiAgICAgICAgY29uc29sZS5lcnJvcihcIkltcGxlbWVudCBXaWxsU2VydmljZSBkZWxldGVcIik7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlU2VjdGlvbihrZXk6c3RyaW5nLCBzZWN0aW9uOlNlY3Rpb24pIHtcclxuXHJcbiAgICAgICAgLy8gY2hlY2sgaWYgZXhpc3RzXHJcblxyXG4gICAgICAgIGlmIChrZXkgaW4gdGhpcy5zZWN0aW9ucykge1xyXG4gICAgICAgICAgICB0aGlzLnNlY3Rpb25zW2tleV0gPSBzZWN0aW9uO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJXaWxsU2VydmljZTogVXBkYXRlIFNlY3Rpb24gRmFpbGVkIHdpdGgga2V5IFwiICsga2V5KTtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcih0aGlzLnNlY3Rpb25zKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc29sZS5sb2coXCJXaWxsU2VydmljZTogVXBkYXRlIFNlY3Rpb24gXCIgKyBrZXkpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiV2lsbFNlcnZpY2U6IFVwZGF0ZSBTZWN0aW9uKHJlc3VsdCkgXCIsIHRoaXMuc2VjdGlvbnNba2V5XSk7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJXaWxsU2VydmljZTogVXBkYXRlIFNlY3Rpb24oQXJyYXkgc3RhdHVzKSBcIiwgdGhpcy5zZWN0aW9ucyk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8vIERlYnVnIGhlbHBlcnM6XHJcbiAgICBwcmludFNlY3Rpb25zKCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuc2VjdGlvbnMpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
