"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by idanhahn on 10/4/2016.
 */
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var Will2Component = (function () {
    function Will2Component() {
        this.will2Form = new forms_1.FormGroup({
            'user-firstName': new forms_1.FormControl(''),
            'user-lastName': new forms_1.FormControl('')
        });
    }
    Will2Component.prototype.onSubmit = function () {
        console.log(this.will2Form);
    };
    Will2Component = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-will2',
            templateUrl: 'will2.component.html',
            styleUrls: ['will2.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], Will2Component);
    return Will2Component;
}());
exports.Will2Component = Will2Component;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvMi93aWxsMi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBOztHQUVHO0FBQ0gscUJBQTBCLGVBQWUsQ0FBQyxDQUFBO0FBQzFDLHNCQUFxQyxnQkFBZ0IsQ0FBQyxDQUFBO0FBUXREO0lBR0k7UUFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksaUJBQVMsQ0FBQztZQUMzQixnQkFBZ0IsRUFBRSxJQUFJLG1CQUFXLENBQUMsRUFBRSxDQUFDO1lBQ3JDLGVBQWUsRUFBRSxJQUFJLG1CQUFXLENBQUMsRUFBRSxDQUFDO1NBQ3ZDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFRCxpQ0FBUSxHQUFSO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQWxCTDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLFVBQVU7WUFDcEIsV0FBVyxFQUFFLHNCQUFzQjtZQUNuQyxTQUFTLEVBQUMsQ0FBQyxxQkFBcUIsQ0FBQztTQUNwQyxDQUFDOztzQkFBQTtJQWlCRixxQkFBQztBQUFELENBaEJBLEFBZ0JDLElBQUE7QUFoQlksc0JBQWMsaUJBZ0IxQixDQUFBIiwiZmlsZSI6IndpbGwvMi93aWxsMi5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICogQ3JlYXRlZCBieSBpZGFuaGFobiBvbiAxMC80LzIwMTYuXHJcbiAqL1xyXG5pbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge0Zvcm1Hcm91cCwgRm9ybUNvbnRyb2x9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdteS13aWxsMicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ3dpbGwyLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczpbJ3dpbGwyLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgV2lsbDJDb21wb25lbnQge1xyXG4gICAgd2lsbDJGb3JtOiBGb3JtR3JvdXA7XHJcbiAgICBcclxuICAgIGNvbnN0cnVjdG9yKCl7XHJcbiAgICAgICAgdGhpcy53aWxsMkZvcm0gPSBuZXcgRm9ybUdyb3VwKHtcclxuICAgICAgICAgICAgJ3VzZXItZmlyc3ROYW1lJzogbmV3IEZvcm1Db250cm9sKCcnKSxcclxuICAgICAgICAgICAgJ3VzZXItbGFzdE5hbWUnOiBuZXcgRm9ybUNvbnRyb2woJycpXHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuICAgIFxyXG4gICAgb25TdWJtaXQoKXtcclxuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLndpbGwyRm9ybSk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIFxyXG4gICAgXHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
