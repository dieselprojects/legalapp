/**
 * Created by idanhahn on 10/20/2016.
 */
"use strict";
var AssetType = (function () {
    function AssetType(type_en, type_heb) {
        this.type_en = type_en;
        this.type_heb = type_heb;
    }
    ;
    return AssetType;
}());
exports.AssetType = AssetType;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvYXNzZXQvYXNzZXQtdHlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7R0FFRzs7QUFFSDtJQUlJLG1CQUFtQixPQUFlLEVBQVMsUUFBZ0I7UUFBeEMsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUFTLGFBQVEsR0FBUixRQUFRLENBQVE7SUFBRSxDQUFDOztJQUNsRSxnQkFBQztBQUFELENBTEEsQUFLQyxJQUFBO0FBTFksaUJBQVMsWUFLckIsQ0FBQSIsImZpbGUiOiJ3aWxsL2Fzc2V0L2Fzc2V0LXR5cGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICogQ3JlYXRlZCBieSBpZGFuaGFobiBvbiAxMC8yMC8yMDE2LlxyXG4gKi9cclxuXHJcbmV4cG9ydCBjbGFzcyBBc3NldFR5cGV7XHJcbiAgICB0eXBlX2VuOiBzdHJpbmc7XHJcbiAgICB0eXBlX2hlYjogc3RyaW5nO1xyXG4gICAgXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdHlwZV9lbjogc3RyaW5nLCBwdWJsaWMgdHlwZV9oZWI6IHN0cmluZyl7fTtcclxufSJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
