"use strict";
/**
 * Created by idanhahn on 10/20/2016.
 */
var AssetFormDescriptor = (function () {
    function AssetFormDescriptor() {
    }
    AssetFormDescriptor.prototype.getAssetTypesHeb = function () {
        var mainAssetsHeb = [];
        for (var _i = 0, _a = this.mainTypes; _i < _a.length; _i++) {
            var assetType = _a[_i];
            mainAssetsHeb.push(assetType.type_heb);
        }
        return mainAssetsHeb;
    };
    AssetFormDescriptor.prototype.getAssetTypeEn = function (assetHeb) {
        for (var _i = 0, _a = this.mainTypes; _i < _a.length; _i++) {
            var assetType = _a[_i];
            if (assetType.type_heb == assetHeb) {
                return assetType.type_en;
            }
        }
    };
    return AssetFormDescriptor;
}());
exports.AssetFormDescriptor = AssetFormDescriptor;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvYXNzZXQvYXNzZXQtZm9ybS1kZXNjcmlwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQTs7R0FFRztBQUVIO0lBS0k7SUFBYyxDQUFDO0lBRWYsOENBQWdCLEdBQWhCO1FBQ0ksSUFBSSxhQUFhLEdBQWtCLEVBQUUsQ0FBQztRQUN0QyxHQUFHLENBQUEsQ0FBa0IsVUFBYyxFQUFkLEtBQUEsSUFBSSxDQUFDLFNBQVMsRUFBZCxjQUFjLEVBQWQsSUFBYyxDQUFDO1lBQWhDLElBQUksU0FBUyxTQUFBO1lBQ2IsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDMUM7UUFDRCxNQUFNLENBQUMsYUFBYSxDQUFDO0lBQ3pCLENBQUM7SUFFRCw0Q0FBYyxHQUFkLFVBQWUsUUFBZ0I7UUFDM0IsR0FBRyxDQUFBLENBQWtCLFVBQWMsRUFBZCxLQUFBLElBQUksQ0FBQyxTQUFTLEVBQWQsY0FBYyxFQUFkLElBQWMsQ0FBQztZQUFoQyxJQUFJLFNBQVMsU0FBQTtZQUNiLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLElBQUksUUFBUSxDQUFDLENBQUEsQ0FBQztnQkFDaEMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUM7WUFDN0IsQ0FBQztTQUNKO0lBRUwsQ0FBQztJQUVMLDBCQUFDO0FBQUQsQ0F4QkEsQUF3QkMsSUFBQTtBQXhCWSwyQkFBbUIsc0JBd0IvQixDQUFBIiwiZmlsZSI6IndpbGwvYXNzZXQvYXNzZXQtZm9ybS1kZXNjcmlwdG9yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBc3NldFR5cGV9IGZyb20gXCIuL2Fzc2V0LXR5cGVcIjtcclxuLyoqXHJcbiAqIENyZWF0ZWQgYnkgaWRhbmhhaG4gb24gMTAvMjAvMjAxNi5cclxuICovXHJcblxyXG5leHBvcnQgY2xhc3MgQXNzZXRGb3JtRGVzY3JpcHRvcntcclxuICAgIFxyXG4gICAgdGl0bGU6IHN0cmluZztcclxuICAgIG1haW5UeXBlczogQXNzZXRUeXBlW107XHJcblxyXG4gICAgY29uc3RydWN0b3IoKXt9XHJcbiAgICBcclxuICAgIGdldEFzc2V0VHlwZXNIZWIoKXtcclxuICAgICAgICBsZXQgbWFpbkFzc2V0c0hlYjogQXJyYXk8c3RyaW5nPiA9IFtdO1xyXG4gICAgICAgIGZvcihsZXQgYXNzZXRUeXBlIG9mIHRoaXMubWFpblR5cGVzKXtcclxuICAgICAgICAgICAgbWFpbkFzc2V0c0hlYi5wdXNoKGFzc2V0VHlwZS50eXBlX2hlYik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBtYWluQXNzZXRzSGViO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBnZXRBc3NldFR5cGVFbihhc3NldEhlYjogc3RyaW5nKXtcclxuICAgICAgICBmb3IobGV0IGFzc2V0VHlwZSBvZiB0aGlzLm1haW5UeXBlcyl7XHJcbiAgICAgICAgICAgIGlmIChhc3NldFR5cGUudHlwZV9oZWIgPT0gYXNzZXRIZWIpe1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGFzc2V0VHlwZS50eXBlX2VuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgfVxyXG4gICAgXHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
