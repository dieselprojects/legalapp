"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by idanhahn on 10/4/2016.
 */
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var will_service_1 = require("../will.service");
//import {Will0FormObject} from "../models/formObj/will-0-form";
var Will0Component = (function () {
    function Will0Component(route, router, willService) {
        this.route = route;
        this.router = router;
        this.willService = willService;
        this.genders = [
            'זכר',
            'נקבה'
        ];
        this.will0Form = new forms_1.FormGroup({
            'user-firstName': new forms_1.FormControl(''),
            'user-lastName': new forms_1.FormControl(''),
            'user-gender': new forms_1.FormControl('זכר'),
            'user-idNumber': new forms_1.FormControl(''),
            'user-email': new forms_1.FormControl('', [
                forms_1.Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
            ])
        });
    }
    Will0Component.prototype.onSubmit = function () {
        console.log(this.will0Form);
        console.log(this.will0Form.get('user-firstName').value);
        /*
        let will0FormObj: Will0FormObject = new Will0FormObject(
            this.will0Form.get('user-firstName').value,
            this.will0Form.get('user-lastName').value,
            this.will0Form.get('user-idNumber').value,
            this.will0Form.get('user-gender').value,
            this.will0Form.get('user-email').value
        );
        
        this.willService.addWill0Form(will0FormObj);
        */
        this.router.navigate(['will', '1']);
    };
    Will0Component = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-will0',
            templateUrl: 'will0.component.html',
            styleUrls: ['will0.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, will_service_1.WillService])
    ], Will0Component);
    return Will0Component;
}());
exports.Will0Component = Will0Component;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvMC93aWxsMC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBOztHQUVHO0FBQ0gscUJBQTBCLGVBQWUsQ0FBQyxDQUFBO0FBQzFDLHNCQUFpRCxnQkFBZ0IsQ0FBQyxDQUFBO0FBQ2xFLHVCQUFxQyxpQkFBaUIsQ0FBQyxDQUFBO0FBQ3ZELDZCQUEwQixpQkFBaUIsQ0FBQyxDQUFBO0FBQzVDLGdFQUFnRTtBQVFoRTtJQU9JLHdCQUNZLEtBQXFCLEVBQ3JCLE1BQWMsRUFDZCxXQUF3QjtRQUZ4QixVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQUNyQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFScEMsWUFBTyxHQUFFO1lBQ0wsS0FBSztZQUNMLE1BQU07U0FDVCxDQUFDO1FBT0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLGlCQUFTLENBQUM7WUFDM0IsZ0JBQWdCLEVBQUcsSUFBSSxtQkFBVyxDQUFDLEVBQUUsQ0FBQztZQUN0QyxlQUFlLEVBQUcsSUFBSSxtQkFBVyxDQUFDLEVBQUUsQ0FBQztZQUNyQyxhQUFhLEVBQUcsSUFBSSxtQkFBVyxDQUFDLEtBQUssQ0FBQztZQUN0QyxlQUFlLEVBQUcsSUFBSSxtQkFBVyxDQUFDLEVBQUUsQ0FBQztZQUNyQyxZQUFZLEVBQUcsSUFBSSxtQkFBVyxDQUFDLEVBQUUsRUFBRTtnQkFDL0Isa0JBQVUsQ0FBQyxPQUFPLENBQUMsdUlBQXVJLENBQUM7YUFDOUosQ0FBQztTQUNMLENBQUMsQ0FBQztJQUNQLENBQUM7SUFHRCxpQ0FBUSxHQUFSO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3hEOzs7Ozs7Ozs7O1VBVUU7UUFDRixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sRUFBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBRXZDLENBQUM7SUE5Q0w7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLFdBQVcsRUFBRSxzQkFBc0I7WUFDbkMsU0FBUyxFQUFDLENBQUMscUJBQXFCLENBQUM7U0FDcEMsQ0FBQzs7c0JBQUE7SUEwQ0YscUJBQUM7QUFBRCxDQXpDQSxBQXlDQyxJQUFBO0FBekNZLHNCQUFjLGlCQXlDMUIsQ0FBQSIsImZpbGUiOiJ3aWxsLzAvd2lsbDAuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiAqIENyZWF0ZWQgYnkgaWRhbmhhaG4gb24gMTAvNC8yMDE2LlxyXG4gKi9cclxuaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtGb3JtR3JvdXAsIEZvcm1Db250cm9sLCBWYWxpZGF0b3JzfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcclxuaW1wb3J0IHtBY3RpdmF0ZWRSb3V0ZSwgUm91dGVyfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7V2lsbFNlcnZpY2V9IGZyb20gXCIuLi93aWxsLnNlcnZpY2VcIjtcclxuLy9pbXBvcnQge1dpbGwwRm9ybU9iamVjdH0gZnJvbSBcIi4uL21vZGVscy9mb3JtT2JqL3dpbGwtMC1mb3JtXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ215LXdpbGwwJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnd2lsbDAuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOlsnd2lsbDAuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBXaWxsMENvbXBvbmVudCB7XHJcbiAgICB3aWxsMEZvcm06IEZvcm1Hcm91cDtcclxuICAgIGdlbmRlcnMgPVtcclxuICAgICAgICAn15bXm9eoJyxcclxuICAgICAgICAn16DXp9eR15QnXHJcbiAgICBdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlLFxyXG4gICAgICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgcHJpdmF0ZSB3aWxsU2VydmljZTogV2lsbFNlcnZpY2VcclxuICAgICl7XHJcbiAgICAgICAgdGhpcy53aWxsMEZvcm0gPSBuZXcgRm9ybUdyb3VwKHtcclxuICAgICAgICAgICAgJ3VzZXItZmlyc3ROYW1lJyA6IG5ldyBGb3JtQ29udHJvbCgnJyksXHJcbiAgICAgICAgICAgICd1c2VyLWxhc3ROYW1lJyA6IG5ldyBGb3JtQ29udHJvbCgnJyksXHJcbiAgICAgICAgICAgICd1c2VyLWdlbmRlcicgOiBuZXcgRm9ybUNvbnRyb2woJ9eW15vXqCcpLFxyXG4gICAgICAgICAgICAndXNlci1pZE51bWJlcicgOiBuZXcgRm9ybUNvbnRyb2woJycpLFxyXG4gICAgICAgICAgICAndXNlci1lbWFpbCcgOiBuZXcgRm9ybUNvbnRyb2woJycsIFtcclxuICAgICAgICAgICAgICAgIFZhbGlkYXRvcnMucGF0dGVybihcIlthLXowLTkhIyQlJicqKy89P15fYHt8fX4tXSsoPzpcXC5bYS16MC05ISMkJSYnKisvPT9eX2B7fH1+LV0rKSpAKD86W2EtejAtOV0oPzpbYS16MC05LV0qW2EtejAtOV0pP1xcLikrW2EtejAtOV0oPzpbYS16MC05LV0qW2EtejAtOV0pP1wiKVxyXG4gICAgICAgICAgICBdKVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBcclxuICAgIG9uU3VibWl0KCl7XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy53aWxsMEZvcm0pO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMud2lsbDBGb3JtLmdldCgndXNlci1maXJzdE5hbWUnKS52YWx1ZSk7XHJcbiAgICAgICAgLypcclxuICAgICAgICBsZXQgd2lsbDBGb3JtT2JqOiBXaWxsMEZvcm1PYmplY3QgPSBuZXcgV2lsbDBGb3JtT2JqZWN0KFxyXG4gICAgICAgICAgICB0aGlzLndpbGwwRm9ybS5nZXQoJ3VzZXItZmlyc3ROYW1lJykudmFsdWUsXHJcbiAgICAgICAgICAgIHRoaXMud2lsbDBGb3JtLmdldCgndXNlci1sYXN0TmFtZScpLnZhbHVlLFxyXG4gICAgICAgICAgICB0aGlzLndpbGwwRm9ybS5nZXQoJ3VzZXItaWROdW1iZXInKS52YWx1ZSxcclxuICAgICAgICAgICAgdGhpcy53aWxsMEZvcm0uZ2V0KCd1c2VyLWdlbmRlcicpLnZhbHVlLFxyXG4gICAgICAgICAgICB0aGlzLndpbGwwRm9ybS5nZXQoJ3VzZXItZW1haWwnKS52YWx1ZVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy53aWxsU2VydmljZS5hZGRXaWxsMEZvcm0od2lsbDBGb3JtT2JqKTtcclxuICAgICAgICAqL1xyXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnd2lsbCcsJzEnXSk7XHJcbiAgICAgICAgXHJcbiAgICB9XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
