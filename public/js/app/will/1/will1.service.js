"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
/**
 * Created by idanhahn on 10/20/2016.
 */
var Will1Service = (function () {
    function Will1Service() {
        // TODO: consider changing to SectionService
        this.assets = [];
    }
    Will1Service.prototype.getAssets = function () {
        return this.assets;
    };
    Will1Service.prototype.addAsset = function (asset) {
        console.log("Will1Service: addAsset");
        this.assets.push(asset);
        console.log(this.assets);
    };
    Will1Service = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], Will1Service);
    return Will1Service;
}());
exports.Will1Service = Will1Service;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvMS93aWxsMS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFFekM7O0dBRUc7QUFJSDtJQUFBO1FBQ0ksNENBQTRDO1FBQzVDLFdBQU0sR0FBVyxFQUFFLENBQUM7SUFjeEIsQ0FBQztJQVhHLGdDQUFTLEdBQVQ7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBR0QsK0JBQVEsR0FBUixVQUFTLEtBQVk7UUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUFmTDtRQUFDLGlCQUFVLEVBQUU7O29CQUFBO0lBaUJiLG1CQUFDO0FBQUQsQ0FoQkEsQUFnQkMsSUFBQTtBQWhCWSxvQkFBWSxlQWdCeEIsQ0FBQSIsImZpbGUiOiJ3aWxsLzEvd2lsbDEuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtBc3NldH0gZnJvbSBcIi4uL21vZGVscy9hc3NldFwiO1xyXG4vKipcclxuICogQ3JlYXRlZCBieSBpZGFuaGFobiBvbiAxMC8yMC8yMDE2LlxyXG4gKi9cclxuXHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBXaWxsMVNlcnZpY2Uge1xyXG4gICAgLy8gVE9ETzogY29uc2lkZXIgY2hhbmdpbmcgdG8gU2VjdGlvblNlcnZpY2VcclxuICAgIGFzc2V0czpBc3NldFtdID0gW107XHJcbiAgICBcclxuICAgIFxyXG4gICAgZ2V0QXNzZXRzKCk6IEFzc2V0W117XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXNzZXRzO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBcclxuICAgIGFkZEFzc2V0KGFzc2V0OiBBc3NldCl7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJXaWxsMVNlcnZpY2U6IGFkZEFzc2V0XCIpO1xyXG4gICAgICAgIHRoaXMuYXNzZXRzLnB1c2goYXNzZXQpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuYXNzZXRzKTtcclxuICAgIH1cclxuICAgIFxyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
