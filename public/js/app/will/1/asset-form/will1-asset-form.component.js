"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var asset_form_descriptor_1 = require("../../asset/asset-form-descriptor");
var forms_1 = require("@angular/forms");
var asset_1 = require("../../models/asset");
var asset_description_1 = require("../../models/asset-description");
var will1_service_1 = require("../will1.service");
var address_1 = require("../../models/address");
var legal_address_1 = require("../../models/legal-address");
var beneficiary_service_1 = require("../../asset-beneficiaries/beneficiary.service");
/**
 * Created by idanhahn on 10/20/2016.
 */
var Will1AssetFormComponent = (function () {
    function Will1AssetFormComponent(benficiaryService, will1Service) {
        this.benficiaryService = benficiaryService;
        this.will1Service = will1Service;
        // value for asset selection dropdown button
        this.selectedAssetHeb = "בחר סוג נכס";
        // starts main beneficiary flow
        this.clickedMainBeneficiary = false;
        console.log("In will1 asset form C");
        this.assetForm = new forms_1.FormGroup({
            'street': new forms_1.FormControl(''),
            'house': new forms_1.FormControl(''),
            'entry': new forms_1.FormControl(''),
            'apartment': new forms_1.FormControl(''),
            'city': new forms_1.FormControl(''),
            'group': new forms_1.FormControl(''),
            'section': new forms_1.FormControl(''),
            'sub_section': new forms_1.FormControl(''),
            'other': new forms_1.FormControl('')
        });
    }
    Will1AssetFormComponent.prototype.ngOnInit = function () {
        this.mainTypeOptionsHeb = this.formDescriptor.getAssetTypesHeb();
        // get all possible beneficiaries
        this.beneficiaries = this.benficiaryService.getBeneficiaries();
    };
    Will1AssetFormComponent.prototype.onSelect = function (selectedIndex) {
        console.log("in on select");
        console.log(selectedIndex);
        this.switch_value = this.switch(selectedIndex);
    };
    Will1AssetFormComponent.prototype.switch = function (selectedIndex) {
        switch (selectedIndex) {
            case "0":
                return "option1";
                break;
            case "1":
                return "option2";
                break;
            case "2":
                return "option1";
                break;
            case "3":
                return "option3";
                break;
            case "4":
                return "option3";
                break;
            case "5":
                return "option4";
                break;
            case "6":
                return "option4";
                break;
            case "7":
                return "option4";
                break;
            case "8":
                return "option5";
                break;
            default:
        }
    };
    Will1AssetFormComponent.prototype.onSubmit = function () {
        console.log("Submit Asset:");
        var asset = this.createAsset("מקרקעין", this.selectedAssetHeb, this.assetForm);
        // check asset type
        // check if main beneficiary exists and if other beneficiaries exists
        this.will1Service.addAsset(asset);
        // reset form
        this.resetForm();
        console.log(asset);
    };
    Will1AssetFormComponent.prototype.resetForm = function () {
        this.assetForm.reset();
        this.selectedAssetHeb = "בחר סוג נכס";
        this.switch_value = null;
    };
    // ------------------- //
    // -- Asset Handler -- //
    // ------------------- //
    Will1AssetFormComponent.prototype.createAsset = function (asset_type, asset_sub_type, form) {
        var asset = new asset_1.Asset();
        asset.block_type = this.getAssetBlockType(form);
        asset.asset_type = asset_type;
        asset.asset_sub_type = asset_sub_type;
        asset.asset_description = this.createAssetDescription(form);
        // TODO: implement both
        // asset.main_beneficiary = this.createMainBeneficiary(form);
        //asset.beneficiaries = this.createBeneficiaries(form);
        return asset;
    };
    // get asset block type
    Will1AssetFormComponent.prototype.getAssetBlockType = function (form) {
        // TODO: add logic here
        // currently returning asset_0 (main beneficiary with additional beneficiaries)
        return "asset_0";
    };
    Will1AssetFormComponent.prototype.createAssetDescription = function (form) {
        var assetDescription = new asset_description_1.AssetDescription();
        assetDescription.block_type = this.getAssetDescriptionBlockType(this.assetForm);
        assetDescription.address = this.createAddress(form);
        assetDescription.legal_address = this.createLegalAddress(form);
        return assetDescription;
    };
    Will1AssetFormComponent.prototype.getAssetDescriptionBlockType = function (form) {
        // TODO: add logic here
        // currentlly returing asset_description_realestate_apartment
        return "asset_description_realestate_apartment";
    };
    Will1AssetFormComponent.prototype.createAddress = function (form) {
        var address = new address_1.Address();
        address.block_type = this.getAddressBlockType(form);
        address.street = form.value.street;
        address.apartment = form.value.apartment;
        address.entry = form.value.entry;
        address.house = form.value.house;
        address.city = form.value.city;
        return address;
    };
    Will1AssetFormComponent.prototype.getAddressBlockType = function (form) {
        // TODO: add logic here
        // currentlly returing address_0
        return "address_0";
    };
    Will1AssetFormComponent.prototype.createLegalAddress = function (form) {
        var legalAddress = new legal_address_1.LegalAddress();
        legalAddress.block_type = this.getLegalAddressBlockType(form);
        legalAddress.group = form.value.group;
        legalAddress.section = form.value.section;
        legalAddress.sub_section = form.value.sub_section;
        return legalAddress;
    };
    Will1AssetFormComponent.prototype.getLegalAddressBlockType = function (form) {
        // TODO: add logic here
        // currentlly returing address_0
        return "legal_address";
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', asset_form_descriptor_1.AssetFormDescriptor)
    ], Will1AssetFormComponent.prototype, "formDescriptor", void 0);
    Will1AssetFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'will1-asset-form',
            templateUrl: 'will1-asset-form.component.html',
            styleUrls: ['will1-asset-form.component.css']
        }), 
        __metadata('design:paramtypes', [beneficiary_service_1.BeneficiaryService, will1_service_1.Will1Service])
    ], Will1AssetFormComponent);
    return Will1AssetFormComponent;
}());
exports.Will1AssetFormComponent = Will1AssetFormComponent;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvMS9hc3NldC1mb3JtL3dpbGwxLWFzc2V0LWZvcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBdUMsZUFBZSxDQUFDLENBQUE7QUFDdkQsc0NBQWtDLG1DQUFtQyxDQUFDLENBQUE7QUFDdEUsc0JBQXFDLGdCQUFnQixDQUFDLENBQUE7QUFDdEQsc0JBQW9CLG9CQUFvQixDQUFDLENBQUE7QUFDekMsa0NBQStCLGdDQUFnQyxDQUFDLENBQUE7QUFDaEUsOEJBQTJCLGtCQUFrQixDQUFDLENBQUE7QUFDOUMsd0JBQXNCLHNCQUFzQixDQUFDLENBQUE7QUFDN0MsOEJBQTJCLDRCQUE0QixDQUFDLENBQUE7QUFFeEQsb0NBQWlDLCtDQUErQyxDQUFDLENBQUE7QUFDakY7O0dBRUc7QUFTSDtJQXFCSSxpQ0FBb0IsaUJBQXFDLEVBQ3JDLFlBQXlCO1FBRHpCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBb0I7UUFDckMsaUJBQVksR0FBWixZQUFZLENBQWE7UUFoQjdDLDRDQUE0QztRQUM1QyxxQkFBZ0IsR0FBVSxhQUFhLENBQUM7UUFTeEMsK0JBQStCO1FBQy9CLDJCQUFzQixHQUFZLEtBQUssQ0FBQztRQU1wQyxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLGlCQUFTLENBQUM7WUFDM0IsUUFBUSxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7WUFDN0IsT0FBTyxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7WUFDNUIsT0FBTyxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7WUFDNUIsV0FBVyxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7WUFDaEMsTUFBTSxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7WUFDM0IsT0FBTyxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7WUFDNUIsU0FBUyxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7WUFDOUIsYUFBYSxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7WUFDbEMsT0FBTyxFQUFFLElBQUksbUJBQVcsQ0FBQyxFQUFFLENBQUM7U0FDL0IsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVELDBDQUFRLEdBQVI7UUFFSSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFBO1FBRWhFLGlDQUFpQztRQUNqQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBR25FLENBQUM7SUFFRCwwQ0FBUSxHQUFSLFVBQVMsYUFBb0I7UUFFekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBR0Qsd0NBQU0sR0FBTixVQUFPLGFBQW9CO1FBRXZCLE1BQU0sQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDcEIsS0FBSyxHQUFHO2dCQUNKLE1BQU0sQ0FBQyxTQUFTLENBQUM7Z0JBQ2pCLEtBQUssQ0FBQztZQUNWLEtBQUssR0FBRztnQkFDSixNQUFNLENBQUMsU0FBUyxDQUFDO2dCQUNqQixLQUFLLENBQUM7WUFDVixLQUFLLEdBQUc7Z0JBQ0osTUFBTSxDQUFDLFNBQVMsQ0FBQztnQkFDakIsS0FBSyxDQUFDO1lBQ1YsS0FBSyxHQUFHO2dCQUNKLE1BQU0sQ0FBQyxTQUFTLENBQUM7Z0JBQ2pCLEtBQUssQ0FBQztZQUNWLEtBQUssR0FBRztnQkFDSixNQUFNLENBQUMsU0FBUyxDQUFDO2dCQUNqQixLQUFLLENBQUM7WUFDVixLQUFLLEdBQUc7Z0JBQ0osTUFBTSxDQUFDLFNBQVMsQ0FBQztnQkFDakIsS0FBSyxDQUFDO1lBQ1YsS0FBSyxHQUFHO2dCQUNKLE1BQU0sQ0FBQyxTQUFTLENBQUM7Z0JBQ2pCLEtBQUssQ0FBQztZQUNWLEtBQUssR0FBRztnQkFDSixNQUFNLENBQUMsU0FBUyxDQUFDO2dCQUNqQixLQUFLLENBQUM7WUFDVixLQUFLLEdBQUc7Z0JBQ0osTUFBTSxDQUFDLFNBQVMsQ0FBQztnQkFDakIsS0FBSyxDQUFDO1lBQ1YsUUFBUTtRQUVaLENBQUM7SUFFTCxDQUFDO0lBRUQsMENBQVEsR0FBUjtRQUVJLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUE7UUFDNUIsSUFBSSxLQUFLLEdBQVMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUVyRixtQkFBbUI7UUFDbkIscUVBQXFFO1FBQ3JFLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRWxDLGFBQWE7UUFDYixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFFakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUV2QixDQUFDO0lBRUQsMkNBQVMsR0FBVDtRQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGFBQWEsQ0FBQztRQUN0QyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztJQUM3QixDQUFDO0lBR0QseUJBQXlCO0lBQ3pCLHlCQUF5QjtJQUN6Qix5QkFBeUI7SUFFekIsNkNBQVcsR0FBWCxVQUFZLFVBQWlCLEVBQUUsY0FBcUIsRUFBRSxJQUFjO1FBRWhFLElBQUksS0FBSyxHQUFTLElBQUksYUFBSyxFQUFFLENBQUM7UUFDOUIsS0FBSyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDaEQsS0FBSyxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDOUIsS0FBSyxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7UUFDdEMsS0FBSyxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1RCx1QkFBdUI7UUFDdkIsNkRBQTZEO1FBQzdELHVEQUF1RDtRQUN2RCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBRWpCLENBQUM7SUFFRCx1QkFBdUI7SUFDdkIsbURBQWlCLEdBQWpCLFVBQWtCLElBQWM7UUFFNUIsdUJBQXVCO1FBQ3ZCLCtFQUErRTtRQUMvRSxNQUFNLENBQUMsU0FBUyxDQUFDO0lBRXJCLENBQUM7SUFFRCx3REFBc0IsR0FBdEIsVUFBdUIsSUFBYztRQUVqQyxJQUFJLGdCQUFnQixHQUFvQixJQUFJLG9DQUFnQixFQUFFLENBQUM7UUFDL0QsZ0JBQWdCLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEYsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEQsZ0JBQWdCLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvRCxNQUFNLENBQUMsZ0JBQWdCLENBQUM7SUFFNUIsQ0FBQztJQUVELDhEQUE0QixHQUE1QixVQUE2QixJQUFjO1FBRXZDLHVCQUF1QjtRQUV2Qiw2REFBNkQ7UUFDN0QsTUFBTSxDQUFDLHdDQUF3QyxDQUFDO0lBRXBELENBQUM7SUFFRCwrQ0FBYSxHQUFiLFVBQWMsSUFBYztRQUN4QixJQUFJLE9BQU8sR0FBVyxJQUFJLGlCQUFPLEVBQUUsQ0FBQztRQUNwQyxPQUFPLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwRCxPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQ25DLE9BQU8sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7UUFDekMsT0FBTyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUNqQyxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ2pDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7UUFDL0IsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUNuQixDQUFDO0lBRUQscURBQW1CLEdBQW5CLFVBQW9CLElBQWM7UUFFOUIsdUJBQXVCO1FBRXZCLGdDQUFnQztRQUNoQyxNQUFNLENBQUMsV0FBVyxDQUFDO0lBRXZCLENBQUM7SUFFRCxvREFBa0IsR0FBbEIsVUFBbUIsSUFBYztRQUM3QixJQUFJLFlBQVksR0FBZ0IsSUFBSSw0QkFBWSxFQUFFLENBQUM7UUFDbkQsWUFBWSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUQsWUFBWSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUN0QyxZQUFZLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1FBQzFDLFlBQVksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7UUFDbEQsTUFBTSxDQUFDLFlBQVksQ0FBQztJQUN4QixDQUFDO0lBRUQsMERBQXdCLEdBQXhCLFVBQXlCLElBQWM7UUFFbkMsdUJBQXVCO1FBRXZCLGdDQUFnQztRQUNoQyxNQUFNLENBQUMsZUFBZSxDQUFDO0lBRTNCLENBQUM7SUFsTUQ7UUFBQyxZQUFLLEVBQUU7O21FQUFBO0lBUlo7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsV0FBVyxFQUFFLGlDQUFpQztZQUM5QyxTQUFTLEVBQUUsQ0FBQyxnQ0FBZ0MsQ0FBQztTQUNoRCxDQUFDOzsrQkFBQTtJQXVNRiw4QkFBQztBQUFELENBdE1BLEFBc01DLElBQUE7QUF0TVksK0JBQXVCLDBCQXNNbkMsQ0FBQSIsImZpbGUiOiJ3aWxsLzEvYXNzZXQtZm9ybS93aWxsMS1hc3NldC1mb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge0Fzc2V0Rm9ybURlc2NyaXB0b3J9IGZyb20gXCIuLi8uLi9hc3NldC9hc3NldC1mb3JtLWRlc2NyaXB0b3JcIjtcclxuaW1wb3J0IHtGb3JtR3JvdXAsIEZvcm1Db250cm9sfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcclxuaW1wb3J0IHtBc3NldH0gZnJvbSBcIi4uLy4uL21vZGVscy9hc3NldFwiO1xyXG5pbXBvcnQge0Fzc2V0RGVzY3JpcHRpb259IGZyb20gXCIuLi8uLi9tb2RlbHMvYXNzZXQtZGVzY3JpcHRpb25cIjtcclxuaW1wb3J0IHtXaWxsMVNlcnZpY2V9IGZyb20gXCIuLi93aWxsMS5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7QWRkcmVzc30gZnJvbSBcIi4uLy4uL21vZGVscy9hZGRyZXNzXCI7XHJcbmltcG9ydCB7TGVnYWxBZGRyZXNzfSBmcm9tIFwiLi4vLi4vbW9kZWxzL2xlZ2FsLWFkZHJlc3NcIjtcclxuaW1wb3J0IHtCZW5lZmljaWFyeX0gZnJvbSBcIi4uLy4uL21vZGVscy9iZW5lZmljaWFyeVwiO1xyXG5pbXBvcnQge0JlbmVmaWNpYXJ5U2VydmljZX0gZnJvbSBcIi4uLy4uL2Fzc2V0LWJlbmVmaWNpYXJpZXMvYmVuZWZpY2lhcnkuc2VydmljZVwiO1xyXG4vKipcclxuICogQ3JlYXRlZCBieSBpZGFuaGFobiBvbiAxMC8yMC8yMDE2LlxyXG4gKi9cclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ3dpbGwxLWFzc2V0LWZvcm0nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICd3aWxsMS1hc3NldC1mb3JtLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWyd3aWxsMS1hc3NldC1mb3JtLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgV2lsbDFBc3NldEZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIGZvcm1EZXNjcmlwdG9yOkFzc2V0Rm9ybURlc2NyaXB0b3I7XHJcblxyXG4gICAgYXNzZXRGb3JtOkZvcm1Hcm91cDtcclxuXHJcbiAgICAvLyB2YWx1ZSBmb3IgYXNzZXQgc2VsZWN0aW9uIGRyb3Bkb3duIGJ1dHRvblxyXG4gICAgc2VsZWN0ZWRBc3NldEhlYjpzdHJpbmcgPSBcIteR15fXqCDXodeV15Ig16DXm9ehXCI7XHJcblxyXG4gICAgLy8gYXNzZXQgc2VsZWN0aW9uIGRyb3AgZG93biB2YWx1ZXNcclxuICAgIG1haW5UeXBlT3B0aW9uc0hlYjpBcnJheTxzdHJpbmc+O1xyXG4gICAgXHJcbiAgICAvLyB2YWx1ZSBmb3IgbmdTd2l0Y2ggc3RhdGVtZW50XHJcbiAgICBzd2l0Y2hfdmFsdWU6IHN0cmluZztcclxuXHJcbiAgICBcclxuICAgIC8vIHN0YXJ0cyBtYWluIGJlbmVmaWNpYXJ5IGZsb3dcclxuICAgIGNsaWNrZWRNYWluQmVuZWZpY2lhcnk6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIFxyXG4gICAgYmVuZWZpY2lhcmllczogQmVuZWZpY2lhcnlbXTtcclxuICAgIFxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBiZW5maWNpYXJ5U2VydmljZTogQmVuZWZpY2lhcnlTZXJ2aWNlICwgXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHdpbGwxU2VydmljZTpXaWxsMVNlcnZpY2UpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkluIHdpbGwxIGFzc2V0IGZvcm0gQ1wiKTtcclxuICAgICAgICB0aGlzLmFzc2V0Rm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xyXG4gICAgICAgICAgICAnc3RyZWV0JzogbmV3IEZvcm1Db250cm9sKCcnKSxcclxuICAgICAgICAgICAgJ2hvdXNlJzogbmV3IEZvcm1Db250cm9sKCcnKSxcclxuICAgICAgICAgICAgJ2VudHJ5JzogbmV3IEZvcm1Db250cm9sKCcnKSxcclxuICAgICAgICAgICAgJ2FwYXJ0bWVudCc6IG5ldyBGb3JtQ29udHJvbCgnJyksXHJcbiAgICAgICAgICAgICdjaXR5JzogbmV3IEZvcm1Db250cm9sKCcnKSxcclxuICAgICAgICAgICAgJ2dyb3VwJzogbmV3IEZvcm1Db250cm9sKCcnKSxcclxuICAgICAgICAgICAgJ3NlY3Rpb24nOiBuZXcgRm9ybUNvbnRyb2woJycpLFxyXG4gICAgICAgICAgICAnc3ViX3NlY3Rpb24nOiBuZXcgRm9ybUNvbnRyb2woJycpLFxyXG4gICAgICAgICAgICAnb3RoZXInOiBuZXcgRm9ybUNvbnRyb2woJycpXHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOnZvaWQge1xyXG5cclxuICAgICAgICB0aGlzLm1haW5UeXBlT3B0aW9uc0hlYiA9IHRoaXMuZm9ybURlc2NyaXB0b3IuZ2V0QXNzZXRUeXBlc0hlYigpXHJcblxyXG4gICAgICAgIC8vIGdldCBhbGwgcG9zc2libGUgYmVuZWZpY2lhcmllc1xyXG4gICAgICAgIHRoaXMuYmVuZWZpY2lhcmllcyA9IHRoaXMuYmVuZmljaWFyeVNlcnZpY2UuZ2V0QmVuZWZpY2lhcmllcygpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgIG9uU2VsZWN0KHNlbGVjdGVkSW5kZXg6U3RyaW5nKSB7XHJcbiAgICAgICAgXHJcbiAgICAgICAgY29uc29sZS5sb2coXCJpbiBvbiBzZWxlY3RcIik7XHJcbiAgICAgICAgY29uc29sZS5sb2coc2VsZWN0ZWRJbmRleCk7XHJcbiAgICAgICAgdGhpcy5zd2l0Y2hfdmFsdWUgPSB0aGlzLnN3aXRjaChzZWxlY3RlZEluZGV4KTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgc3dpdGNoKHNlbGVjdGVkSW5kZXg6U3RyaW5nKTpzdHJpbmcge1xyXG4gICAgICAgIFxyXG4gICAgICAgIHN3aXRjaCAoc2VsZWN0ZWRJbmRleCkge1xyXG4gICAgICAgICAgICBjYXNlIFwiMFwiOiAvLyBhcGFydG1lbnRcclxuICAgICAgICAgICAgICAgIHJldHVybiBcIm9wdGlvbjFcIjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFwiMVwiOiAvLyBnZW5lcmFsIGhvdXNlXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJvcHRpb24yXCI7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBcIjJcIjogLy8gXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJvcHRpb24xXCI7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBcIjNcIjpcclxuICAgICAgICAgICAgICAgIHJldHVybiBcIm9wdGlvbjNcIjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFwiNFwiOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwib3B0aW9uM1wiO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgXCI1XCI6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJvcHRpb240XCI7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBcIjZcIjpcclxuICAgICAgICAgICAgICAgIHJldHVybiBcIm9wdGlvbjRcIjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFwiN1wiOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwib3B0aW9uNFwiO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgXCI4XCI6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJvcHRpb241XCI7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBvblN1Ym1pdCgpIHtcclxuXHJcbiAgICAgICAgY29uc29sZS5sb2coXCJTdWJtaXQgQXNzZXQ6XCIpXHJcbiAgICAgICAgbGV0IGFzc2V0OkFzc2V0ID0gdGhpcy5jcmVhdGVBc3NldChcItee16fXqNen16LXmdefXCIsIHRoaXMuc2VsZWN0ZWRBc3NldEhlYiwgdGhpcy5hc3NldEZvcm0pO1xyXG5cclxuICAgICAgICAvLyBjaGVjayBhc3NldCB0eXBlXHJcbiAgICAgICAgLy8gY2hlY2sgaWYgbWFpbiBiZW5lZmljaWFyeSBleGlzdHMgYW5kIGlmIG90aGVyIGJlbmVmaWNpYXJpZXMgZXhpc3RzXHJcbiAgICAgICAgdGhpcy53aWxsMVNlcnZpY2UuYWRkQXNzZXQoYXNzZXQpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8vIHJlc2V0IGZvcm1cclxuICAgICAgICB0aGlzLnJlc2V0Rm9ybSgpO1xyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhhc3NldCk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0Rm9ybSgpe1xyXG4gICAgICAgIHRoaXMuYXNzZXRGb3JtLnJlc2V0KCk7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEFzc2V0SGViID0gXCLXkdeX16gg16HXldeSINeg15vXoVwiO1xyXG4gICAgICAgIHRoaXMuc3dpdGNoX3ZhbHVlID0gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBcclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0gLy9cclxuICAgIC8vIC0tIEFzc2V0IEhhbmRsZXIgLS0gLy9cclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0gLy9cclxuXHJcbiAgICBjcmVhdGVBc3NldChhc3NldF90eXBlOnN0cmluZywgYXNzZXRfc3ViX3R5cGU6c3RyaW5nLCBmb3JtOkZvcm1Hcm91cCk6QXNzZXQge1xyXG5cclxuICAgICAgICBsZXQgYXNzZXQ6QXNzZXQgPSBuZXcgQXNzZXQoKTtcclxuICAgICAgICBhc3NldC5ibG9ja190eXBlID0gdGhpcy5nZXRBc3NldEJsb2NrVHlwZShmb3JtKTtcclxuICAgICAgICBhc3NldC5hc3NldF90eXBlID0gYXNzZXRfdHlwZTtcclxuICAgICAgICBhc3NldC5hc3NldF9zdWJfdHlwZSA9IGFzc2V0X3N1Yl90eXBlO1xyXG4gICAgICAgIGFzc2V0LmFzc2V0X2Rlc2NyaXB0aW9uID0gdGhpcy5jcmVhdGVBc3NldERlc2NyaXB0aW9uKGZvcm0pO1xyXG4gICAgICAgIC8vIFRPRE86IGltcGxlbWVudCBib3RoXHJcbiAgICAgICAgLy8gYXNzZXQubWFpbl9iZW5lZmljaWFyeSA9IHRoaXMuY3JlYXRlTWFpbkJlbmVmaWNpYXJ5KGZvcm0pO1xyXG4gICAgICAgIC8vYXNzZXQuYmVuZWZpY2lhcmllcyA9IHRoaXMuY3JlYXRlQmVuZWZpY2lhcmllcyhmb3JtKTtcclxuICAgICAgICByZXR1cm4gYXNzZXQ7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8vIGdldCBhc3NldCBibG9jayB0eXBlXHJcbiAgICBnZXRBc3NldEJsb2NrVHlwZShmb3JtOkZvcm1Hcm91cCk6c3RyaW5nIHtcclxuXHJcbiAgICAgICAgLy8gVE9ETzogYWRkIGxvZ2ljIGhlcmVcclxuICAgICAgICAvLyBjdXJyZW50bHkgcmV0dXJuaW5nIGFzc2V0XzAgKG1haW4gYmVuZWZpY2lhcnkgd2l0aCBhZGRpdGlvbmFsIGJlbmVmaWNpYXJpZXMpXHJcbiAgICAgICAgcmV0dXJuIFwiYXNzZXRfMFwiO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVBc3NldERlc2NyaXB0aW9uKGZvcm06Rm9ybUdyb3VwKTpBc3NldERlc2NyaXB0aW9uIHtcclxuXHJcbiAgICAgICAgbGV0IGFzc2V0RGVzY3JpcHRpb246QXNzZXREZXNjcmlwdGlvbiA9IG5ldyBBc3NldERlc2NyaXB0aW9uKCk7XHJcbiAgICAgICAgYXNzZXREZXNjcmlwdGlvbi5ibG9ja190eXBlID0gdGhpcy5nZXRBc3NldERlc2NyaXB0aW9uQmxvY2tUeXBlKHRoaXMuYXNzZXRGb3JtKTtcclxuICAgICAgICBhc3NldERlc2NyaXB0aW9uLmFkZHJlc3MgPSB0aGlzLmNyZWF0ZUFkZHJlc3MoZm9ybSk7XHJcbiAgICAgICAgYXNzZXREZXNjcmlwdGlvbi5sZWdhbF9hZGRyZXNzID0gdGhpcy5jcmVhdGVMZWdhbEFkZHJlc3MoZm9ybSk7XHJcbiAgICAgICAgcmV0dXJuIGFzc2V0RGVzY3JpcHRpb247XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGdldEFzc2V0RGVzY3JpcHRpb25CbG9ja1R5cGUoZm9ybTpGb3JtR3JvdXApOnN0cmluZyB7XHJcblxyXG4gICAgICAgIC8vIFRPRE86IGFkZCBsb2dpYyBoZXJlXHJcblxyXG4gICAgICAgIC8vIGN1cnJlbnRsbHkgcmV0dXJpbmcgYXNzZXRfZGVzY3JpcHRpb25fcmVhbGVzdGF0ZV9hcGFydG1lbnRcclxuICAgICAgICByZXR1cm4gXCJhc3NldF9kZXNjcmlwdGlvbl9yZWFsZXN0YXRlX2FwYXJ0bWVudFwiO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVBZGRyZXNzKGZvcm06Rm9ybUdyb3VwKTpBZGRyZXNzIHtcclxuICAgICAgICBsZXQgYWRkcmVzczpBZGRyZXNzID0gbmV3IEFkZHJlc3MoKTtcclxuICAgICAgICBhZGRyZXNzLmJsb2NrX3R5cGUgPSB0aGlzLmdldEFkZHJlc3NCbG9ja1R5cGUoZm9ybSk7XHJcbiAgICAgICAgYWRkcmVzcy5zdHJlZXQgPSBmb3JtLnZhbHVlLnN0cmVldDtcclxuICAgICAgICBhZGRyZXNzLmFwYXJ0bWVudCA9IGZvcm0udmFsdWUuYXBhcnRtZW50O1xyXG4gICAgICAgIGFkZHJlc3MuZW50cnkgPSBmb3JtLnZhbHVlLmVudHJ5O1xyXG4gICAgICAgIGFkZHJlc3MuaG91c2UgPSBmb3JtLnZhbHVlLmhvdXNlO1xyXG4gICAgICAgIGFkZHJlc3MuY2l0eSA9IGZvcm0udmFsdWUuY2l0eTtcclxuICAgICAgICByZXR1cm4gYWRkcmVzcztcclxuICAgIH1cclxuXHJcbiAgICBnZXRBZGRyZXNzQmxvY2tUeXBlKGZvcm06Rm9ybUdyb3VwKTpzdHJpbmcge1xyXG5cclxuICAgICAgICAvLyBUT0RPOiBhZGQgbG9naWMgaGVyZVxyXG5cclxuICAgICAgICAvLyBjdXJyZW50bGx5IHJldHVyaW5nIGFkZHJlc3NfMFxyXG4gICAgICAgIHJldHVybiBcImFkZHJlc3NfMFwiO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVMZWdhbEFkZHJlc3MoZm9ybTpGb3JtR3JvdXApOkxlZ2FsQWRkcmVzcyB7XHJcbiAgICAgICAgbGV0IGxlZ2FsQWRkcmVzczpMZWdhbEFkZHJlc3MgPSBuZXcgTGVnYWxBZGRyZXNzKCk7XHJcbiAgICAgICAgbGVnYWxBZGRyZXNzLmJsb2NrX3R5cGUgPSB0aGlzLmdldExlZ2FsQWRkcmVzc0Jsb2NrVHlwZShmb3JtKTtcclxuICAgICAgICBsZWdhbEFkZHJlc3MuZ3JvdXAgPSBmb3JtLnZhbHVlLmdyb3VwO1xyXG4gICAgICAgIGxlZ2FsQWRkcmVzcy5zZWN0aW9uID0gZm9ybS52YWx1ZS5zZWN0aW9uO1xyXG4gICAgICAgIGxlZ2FsQWRkcmVzcy5zdWJfc2VjdGlvbiA9IGZvcm0udmFsdWUuc3ViX3NlY3Rpb247XHJcbiAgICAgICAgcmV0dXJuIGxlZ2FsQWRkcmVzcztcclxuICAgIH1cclxuXHJcbiAgICBnZXRMZWdhbEFkZHJlc3NCbG9ja1R5cGUoZm9ybTpGb3JtR3JvdXApOnN0cmluZyB7XHJcblxyXG4gICAgICAgIC8vIFRPRE86IGFkZCBsb2dpYyBoZXJlXHJcblxyXG4gICAgICAgIC8vIGN1cnJlbnRsbHkgcmV0dXJpbmcgYWRkcmVzc18wXHJcbiAgICAgICAgcmV0dXJuIFwibGVnYWxfYWRkcmVzc1wiO1xyXG5cclxuICAgIH1cclxuXHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
