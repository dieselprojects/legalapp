"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by idanhahn on 10/4/2016.
 */
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var will_service_1 = require("../will.service");
var asset_type_1 = require("../asset/asset-type");
var asset_form_descriptor_1 = require("../asset/asset-form-descriptor");
var will1_service_1 = require("./will1.service");
var Will1Component = (function () {
    function Will1Component(willService, will1Service) {
        this.willService = willService;
        this.will1Service = will1Service;
        this.assetFormDescriptor = new asset_form_descriptor_1.AssetFormDescriptor();
        this.sectionForm = new forms_1.FormGroup({
            'street': new forms_1.FormControl(''),
            '': new forms_1.FormControl('')
        });
        this.assetFormDescriptor.title = "נכס מקרקעין";
        this.assetFormDescriptor.mainTypes = [
            new asset_type_1.AssetType("house0", "דירה"),
            new asset_type_1.AssetType("house1", "בית פרטי"),
            new asset_type_1.AssetType("house2", "בית"),
            new asset_type_1.AssetType("store0", "חנות"),
            new asset_type_1.AssetType("office0", "משרד"),
            new asset_type_1.AssetType("field0", "שטח"),
            new asset_type_1.AssetType("field1", "שטח חקלאי"),
            new asset_type_1.AssetType("field2", "חניה"),
            new asset_type_1.AssetType("other", "אחר")
        ];
        console.log("in C will1Component");
        console.log(this.assetFormDescriptor);
    }
    Will1Component.prototype.ngOnInit = function () {
        this.assets = this.will1Service.getAssets();
    };
    // new implementation
    Will1Component.prototype.submitSection = function () {
    };
    Will1Component.prototype.onSubmit = function () {
        // create new Asset
        // if user logged in save asset to DB
        console.log(this.sectionForm);
    };
    Will1Component = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-will1',
            templateUrl: 'will1.component.html',
            styleUrls: ['will1.component.css']
        }), 
        __metadata('design:paramtypes', [will_service_1.WillService, will1_service_1.Will1Service])
    ], Will1Component);
    return Will1Component;
}());
exports.Will1Component = Will1Component;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvMS93aWxsMS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBOztHQUVHO0FBQ0gscUJBQWdDLGVBQWUsQ0FBQyxDQUFBO0FBQ2hELHNCQUFxQyxnQkFBZ0IsQ0FBQyxDQUFBO0FBQ3RELDZCQUEwQixpQkFBaUIsQ0FBQyxDQUFBO0FBRTVDLDJCQUF3QixxQkFBcUIsQ0FBQyxDQUFBO0FBQzlDLHNDQUFrQyxnQ0FBZ0MsQ0FBQyxDQUFBO0FBQ25FLDhCQUEyQixpQkFBaUIsQ0FBQyxDQUFBO0FBUTdDO0lBU0ksd0JBQW9CLFdBQXVCLEVBQVUsWUFBMEI7UUFBM0QsZ0JBQVcsR0FBWCxXQUFXLENBQVk7UUFBVSxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUovRSx3QkFBbUIsR0FBd0IsSUFBSSwyQ0FBbUIsRUFBRSxDQUFDO1FBS2pFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxpQkFBUyxDQUFDO1lBQzdCLFFBQVEsRUFBRSxJQUFJLG1CQUFXLENBQUMsRUFBRSxDQUFDO1lBQzdCLEVBQUUsRUFBRSxJQUFJLG1CQUFXLENBQUMsRUFBRSxDQUFDO1NBQzFCLENBQUMsQ0FBQTtRQUNGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFBO1FBRTlDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEdBQUc7WUFDakMsSUFBSSxzQkFBUyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUM7WUFDL0IsSUFBSSxzQkFBUyxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUM7WUFDbkMsSUFBSSxzQkFBUyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUM7WUFDOUIsSUFBSSxzQkFBUyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUM7WUFDL0IsSUFBSSxzQkFBUyxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUM7WUFDaEMsSUFBSSxzQkFBUyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUM7WUFDOUIsSUFBSSxzQkFBUyxDQUFDLFFBQVEsRUFBRSxXQUFXLENBQUM7WUFDcEMsSUFBSSxzQkFBUyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUM7WUFDL0IsSUFBSSxzQkFBUyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUM7U0FDaEMsQ0FBQztRQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQTtRQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFRCxpQ0FBUSxHQUFSO1FBRUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBRS9DLENBQUM7SUFHRCxxQkFBcUI7SUFFckIsc0NBQWEsR0FBYjtJQUVBLENBQUM7SUFHRCxpQ0FBUSxHQUFSO1FBRUksbUJBQW1CO1FBQ25CLHFDQUFxQztRQUdyQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUdsQyxDQUFDO0lBNURMO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsVUFBVTtZQUNwQixXQUFXLEVBQUUsc0JBQXNCO1lBQ25DLFNBQVMsRUFBRSxDQUFDLHFCQUFxQixDQUFDO1NBQ3JDLENBQUM7O3NCQUFBO0lBK0RGLHFCQUFDO0FBQUQsQ0E5REEsQUE4REMsSUFBQTtBQTlEWSxzQkFBYyxpQkE4RDFCLENBQUEiLCJmaWxlIjoid2lsbC8xL3dpbGwxLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBDcmVhdGVkIGJ5IGlkYW5oYWhuIG9uIDEwLzQvMjAxNi5cclxuICovXHJcbmltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7Rm9ybUdyb3VwLCBGb3JtQ29udHJvbH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcbmltcG9ydCB7V2lsbFNlcnZpY2V9IGZyb20gXCIuLi93aWxsLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtBc3NldH0gZnJvbSBcIi4uL21vZGVscy9hc3NldFwiO1xyXG5pbXBvcnQge0Fzc2V0VHlwZX0gZnJvbSBcIi4uL2Fzc2V0L2Fzc2V0LXR5cGVcIjtcclxuaW1wb3J0IHtBc3NldEZvcm1EZXNjcmlwdG9yfSBmcm9tIFwiLi4vYXNzZXQvYXNzZXQtZm9ybS1kZXNjcmlwdG9yXCI7XHJcbmltcG9ydCB7V2lsbDFTZXJ2aWNlfSBmcm9tIFwiLi93aWxsMS5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ215LXdpbGwxJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnd2lsbDEuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJ3dpbGwxLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgV2lsbDFDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXR7XHJcblxyXG5cclxuICAgIHNlY3Rpb25Gb3JtOkZvcm1Hcm91cDtcclxuICAgIFxyXG4gICAgYXNzZXRGb3JtRGVzY3JpcHRvcjogQXNzZXRGb3JtRGVzY3JpcHRvciA9IG5ldyBBc3NldEZvcm1EZXNjcmlwdG9yKCk7XHJcblxyXG4gICAgYXNzZXRzOiBBc3NldFtdOyAgICBcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHdpbGxTZXJ2aWNlOldpbGxTZXJ2aWNlLCBwcml2YXRlIHdpbGwxU2VydmljZTogV2lsbDFTZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy5zZWN0aW9uRm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xyXG4gICAgICAgICAgICAnc3RyZWV0JzogbmV3IEZvcm1Db250cm9sKCcnKSxcclxuICAgICAgICAgICAgJyc6IG5ldyBGb3JtQ29udHJvbCgnJylcclxuICAgICAgICB9KVxyXG4gICAgICAgIHRoaXMuYXNzZXRGb3JtRGVzY3JpcHRvci50aXRsZSA9IFwi16DXm9ehINee16fXqNen16LXmdefXCJcclxuXHJcbiAgICAgICAgdGhpcy5hc3NldEZvcm1EZXNjcmlwdG9yLm1haW5UeXBlcyA9IFtcclxuICAgICAgICAgICAgbmV3IEFzc2V0VHlwZShcImhvdXNlMFwiLCBcIteT15nXqNeUXCIpLFxyXG4gICAgICAgICAgICBuZXcgQXNzZXRUeXBlKFwiaG91c2UxXCIsIFwi15HXmdeqINek16jXmNeZXCIpLFxyXG4gICAgICAgICAgICBuZXcgQXNzZXRUeXBlKFwiaG91c2UyXCIsIFwi15HXmdeqXCIpLFxyXG4gICAgICAgICAgICBuZXcgQXNzZXRUeXBlKFwic3RvcmUwXCIsIFwi15fXoNeV16pcIiksXHJcbiAgICAgICAgICAgIG5ldyBBc3NldFR5cGUoXCJvZmZpY2UwXCIsIFwi157Xqdeo15NcIiksXHJcbiAgICAgICAgICAgIG5ldyBBc3NldFR5cGUoXCJmaWVsZDBcIiwgXCLXqdeY15dcIiksXHJcbiAgICAgICAgICAgIG5ldyBBc3NldFR5cGUoXCJmaWVsZDFcIiwgXCLXqdeY15cg15fXp9ec15DXmVwiKSxcclxuICAgICAgICAgICAgbmV3IEFzc2V0VHlwZShcImZpZWxkMlwiLCBcIteX16DXmdeUXCIpLFxyXG4gICAgICAgICAgICBuZXcgQXNzZXRUeXBlKFwib3RoZXJcIiwgXCLXkNeX16hcIilcclxuICAgICAgICBdO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiaW4gQyB3aWxsMUNvbXBvbmVudFwiKVxyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuYXNzZXRGb3JtRGVzY3JpcHRvcik7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTp2b2lkIHtcclxuICAgICAgICBcclxuICAgICAgIHRoaXMuYXNzZXRzID0gdGhpcy53aWxsMVNlcnZpY2UuZ2V0QXNzZXRzKCk7IFxyXG4gICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgIFxyXG4gICAgLy8gbmV3IGltcGxlbWVudGF0aW9uXHJcblxyXG4gICAgc3VibWl0U2VjdGlvbigpe1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG5cclxuXHJcbiAgICBvblN1Ym1pdCgpIHtcclxuXHJcbiAgICAgICAgLy8gY3JlYXRlIG5ldyBBc3NldFxyXG4gICAgICAgIC8vIGlmIHVzZXIgbG9nZ2VkIGluIHNhdmUgYXNzZXQgdG8gREJcclxuXHJcblxyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuc2VjdGlvbkZvcm0pO1xyXG5cclxuXHJcbiAgICB9XHJcblxyXG4gICAgXHJcbiAgICBcclxuICAgIFxyXG4gICAgXHJcbiAgICBcclxuICAgIFxyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
