"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by idanhahn on 10/4/2016.
 */
var core_1 = require("@angular/core");
var WillComponent = (function () {
    function WillComponent() {
    }
    WillComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-will',
            templateUrl: 'will.component.html',
            styleUrls: ['will.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], WillComponent);
    return WillComponent;
}());
exports.WillComponent = WillComponent;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpbGwvd2lsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBOztHQUVHO0FBQ0gscUJBQTBCLGVBQWUsQ0FBQyxDQUFBO0FBUzFDO0lBQUE7SUFDQSxDQUFDO0lBUkQ7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxTQUFTO1lBQ25CLFdBQVcsRUFBRSxxQkFBcUI7WUFDbEMsU0FBUyxFQUFDLENBQUMsb0JBQW9CLENBQUM7U0FFbkMsQ0FBQzs7cUJBQUE7SUFFRixvQkFBQztBQUFELENBREEsQUFDQyxJQUFBO0FBRFkscUJBQWEsZ0JBQ3pCLENBQUEiLCJmaWxlIjoid2lsbC93aWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBDcmVhdGVkIGJ5IGlkYW5oYWhuIG9uIDEwLzQvMjAxNi5cclxuICovXHJcbmltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ215LXdpbGwnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICd3aWxsLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczpbJ3dpbGwuY29tcG9uZW50LmNzcyddXHJcbiAgICBcclxufSlcclxuZXhwb3J0IGNsYXNzIFdpbGxDb21wb25lbnQge1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
