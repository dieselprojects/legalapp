"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var common_1 = require('@angular/common');
var app_component_1 = require("./app.component");
var header_component_1 = require("./header/header.component");
var app_routing_1 = require("./app.routing");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var will_component_1 = require("./will/will.component");
var will1_component_1 = require("./will/1/will1.component");
var will0_component_1 = require("./will/0/will0.component");
var will_service_1 = require("./will/will.service");
var http_service_1 = require("./home/http.service");
var will_asset_component_1 = require("./will/will-asset.component");
var will1_asset_form_component_1 = require("./will/1/asset-form/will1-asset-form.component");
var will1_service_1 = require("./will/1/will1.service");
var beneficiary_main_component_1 = require("./will/asset-beneficiaries/beneficiary-main/beneficiary-main.component");
var beneficiary_component_1 = require("./will/asset-beneficiaries/beneficiary/beneficiary.component");
var beneficiary_service_1 = require("./will/asset-beneficiaries/beneficiary.service");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                header_component_1.HeaderComponent,
                will_component_1.WillComponent,
                will0_component_1.Will0Component,
                will1_component_1.Will1Component,
                will_asset_component_1.WillAssetComponent,
                will1_asset_form_component_1.Will1AssetFormComponent,
                beneficiary_main_component_1.BeneficiaryMainComponent,
                beneficiary_component_1.BeneficiaryComponent
            ],
            imports: [platform_browser_1.BrowserModule, app_routing_1.routing, forms_1.FormsModule, forms_1.ReactiveFormsModule, http_1.HttpModule],
            bootstrap: [app_component_1.AppComponent],
            providers: [core_1.provide(common_1.LocationStrategy, { useClass: common_1.HashLocationStrategy }), will_service_1.WillService, http_service_1.HttpService, will1_service_1.Will1Service, beneficiary_service_1.BeneficiaryService]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFnQyxlQUFlLENBQUMsQ0FBQTtBQUNoRCxpQ0FBNEIsMkJBQTJCLENBQUMsQ0FBQTtBQUV4RCx1QkFBcUQsaUJBQWlCLENBQUMsQ0FBQTtBQUV2RSw4QkFBMkIsaUJBQWlCLENBQUMsQ0FBQTtBQUU3QyxpQ0FBOEIsMkJBQTJCLENBQUMsQ0FBQTtBQUUxRCw0QkFBc0IsZUFBZSxDQUFDLENBQUE7QUFDdEMsc0JBQStDLGdCQUFnQixDQUFDLENBQUE7QUFDaEUscUJBQXlCLGVBQWUsQ0FBQyxDQUFBO0FBQ3pDLCtCQUE0Qix1QkFBdUIsQ0FBQyxDQUFBO0FBQ3BELGdDQUE2QiwwQkFBMEIsQ0FBQyxDQUFBO0FBQ3hELGdDQUE2QiwwQkFBMEIsQ0FBQyxDQUFBO0FBQ3hELDZCQUEwQixxQkFBcUIsQ0FBQyxDQUFBO0FBQ2hELDZCQUEwQixxQkFBcUIsQ0FBQyxDQUFBO0FBQ2hELHFDQUFpQyw2QkFBNkIsQ0FBQyxDQUFBO0FBQy9ELDJDQUFzQyxnREFBZ0QsQ0FBQyxDQUFBO0FBQ3ZGLDhCQUEyQix3QkFBd0IsQ0FBQyxDQUFBO0FBQ3BELDJDQUF1Qyx3RUFBd0UsQ0FBQyxDQUFBO0FBQ2hILHNDQUFtQyw4REFBOEQsQ0FBQyxDQUFBO0FBQ2xHLG9DQUFpQyxnREFBZ0QsQ0FBQyxDQUFBO0FBa0JsRjtJQUFBO0lBRUEsQ0FBQztJQWxCRDtRQUFDLGVBQVEsQ0FBQztZQUNOLFlBQVksRUFBRTtnQkFDViw0QkFBWTtnQkFDWixrQ0FBZTtnQkFDZiw4QkFBYTtnQkFDYixnQ0FBYztnQkFDZCxnQ0FBYztnQkFDZCx5Q0FBa0I7Z0JBQ2xCLG9EQUF1QjtnQkFDdkIscURBQXdCO2dCQUN4Qiw0Q0FBb0I7YUFDdkI7WUFDRCxPQUFPLEVBQUUsQ0FBQyxnQ0FBYSxFQUFFLHFCQUFPLEVBQUUsbUJBQVcsRUFBRSwyQkFBbUIsRUFBRSxpQkFBVSxDQUFDO1lBQy9FLFNBQVMsRUFBRSxDQUFDLDRCQUFZLENBQUM7WUFDekIsU0FBUyxFQUFFLENBQUMsY0FBTyxDQUFDLHlCQUFnQixFQUFFLEVBQUMsUUFBUSxFQUFFLDZCQUFvQixFQUFDLENBQUMsRUFBRSwwQkFBVyxFQUFFLDBCQUFXLEVBQUUsNEJBQVksRUFBRSx3Q0FBa0IsQ0FBQztTQUN2SSxDQUFDOztpQkFBQTtJQUdGLGdCQUFDO0FBQUQsQ0FGQSxBQUVDLElBQUE7QUFGWSxpQkFBUyxZQUVyQixDQUFBIiwiZmlsZSI6ImFwcC5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlLCBwcm92aWRlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtCcm93c2VyTW9kdWxlfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcclxuXHJcbmltcG9ydCB7TG9jYXRpb25TdHJhdGVneSwgSGFzaExvY2F0aW9uU3RyYXRlZ3l9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5pbXBvcnQge0FwcENvbXBvbmVudH0gZnJvbSBcIi4vYXBwLmNvbXBvbmVudFwiO1xyXG5cclxuaW1wb3J0IHtIZWFkZXJDb21wb25lbnR9IGZyb20gXCIuL2hlYWRlci9oZWFkZXIuY29tcG9uZW50XCI7XHJcblxyXG5pbXBvcnQge3JvdXRpbmd9IGZyb20gXCIuL2FwcC5yb3V0aW5nXCI7XHJcbmltcG9ydCB7Rm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGV9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xyXG5pbXBvcnQge0h0dHBNb2R1bGV9IGZyb20gXCJAYW5ndWxhci9odHRwXCI7XHJcbmltcG9ydCB7V2lsbENvbXBvbmVudH0gZnJvbSBcIi4vd2lsbC93aWxsLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQge1dpbGwxQ29tcG9uZW50fSBmcm9tIFwiLi93aWxsLzEvd2lsbDEuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7V2lsbDBDb21wb25lbnR9IGZyb20gXCIuL3dpbGwvMC93aWxsMC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtXaWxsU2VydmljZX0gZnJvbSBcIi4vd2lsbC93aWxsLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtIdHRwU2VydmljZX0gZnJvbSBcIi4vaG9tZS9odHRwLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtXaWxsQXNzZXRDb21wb25lbnR9IGZyb20gXCIuL3dpbGwvd2lsbC1hc3NldC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtXaWxsMUFzc2V0Rm9ybUNvbXBvbmVudH0gZnJvbSBcIi4vd2lsbC8xL2Fzc2V0LWZvcm0vd2lsbDEtYXNzZXQtZm9ybS5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtXaWxsMVNlcnZpY2V9IGZyb20gXCIuL3dpbGwvMS93aWxsMS5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7QmVuZWZpY2lhcnlNYWluQ29tcG9uZW50fSBmcm9tIFwiLi93aWxsL2Fzc2V0LWJlbmVmaWNpYXJpZXMvYmVuZWZpY2lhcnktbWFpbi9iZW5lZmljaWFyeS1tYWluLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQge0JlbmVmaWNpYXJ5Q29tcG9uZW50fSBmcm9tIFwiLi93aWxsL2Fzc2V0LWJlbmVmaWNpYXJpZXMvYmVuZWZpY2lhcnkvYmVuZWZpY2lhcnkuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7QmVuZWZpY2lhcnlTZXJ2aWNlfSBmcm9tIFwiLi93aWxsL2Fzc2V0LWJlbmVmaWNpYXJpZXMvYmVuZWZpY2lhcnkuc2VydmljZVwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIEFwcENvbXBvbmVudCxcclxuICAgICAgICBIZWFkZXJDb21wb25lbnQsXHJcbiAgICAgICAgV2lsbENvbXBvbmVudCxcclxuICAgICAgICBXaWxsMENvbXBvbmVudCxcclxuICAgICAgICBXaWxsMUNvbXBvbmVudCxcclxuICAgICAgICBXaWxsQXNzZXRDb21wb25lbnQsXHJcbiAgICAgICAgV2lsbDFBc3NldEZvcm1Db21wb25lbnQsXHJcbiAgICAgICAgQmVuZWZpY2lhcnlNYWluQ29tcG9uZW50LFxyXG4gICAgICAgIEJlbmVmaWNpYXJ5Q29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgaW1wb3J0czogW0Jyb3dzZXJNb2R1bGUsIHJvdXRpbmcsIEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlLCBIdHRwTW9kdWxlXSxcclxuICAgIGJvb3RzdHJhcDogW0FwcENvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtwcm92aWRlKExvY2F0aW9uU3RyYXRlZ3ksIHt1c2VDbGFzczogSGFzaExvY2F0aW9uU3RyYXRlZ3l9KSwgV2lsbFNlcnZpY2UsIEh0dHBTZXJ2aWNlLCBXaWxsMVNlcnZpY2UsIEJlbmVmaWNpYXJ5U2VydmljZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIEFwcE1vZHVsZSB7XHJcblxyXG59Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
